import {observable} from 'mobx';
import {EventEmitter} from 'events';

class SocketConnection extends EventEmitter {
  @observable status = 'pending';
  @observable authenticated = false;

  constructor(endpoint) {
    super();

    this.endpoint = `ws://${endpoint}`;
    this.authToken = null;

    this.connect();
  }

  bindEvents() {
    const socket = this.socket;

    socket.addEventListener('message', this.handleMessage);
    socket.addEventListener('open', this.handleConnect);
    socket.addEventListener('close', this.handleDisconnect);
    socket.addEventListener('error', this.handleDisconnect);
  }

  unbindEvents() {
    const socket = this.socket;

    socket.removeEventListener('message', this.handleMessage);
    socket.removeEventListener('open', this.handleConnect);
    socket.removeEventListener('close', this.handleDisconnect);
    socket.removeEventListener('error', this.handleDisconnect);
  }

  /**
  * Handle a connection event
  **/
  @bind
  handleConnect() {
    if (this.authToken) this.authenticate();
    this.status = 'connected';
  }

  /**
  * Handle a disconnection event
  **/
  @bind
  handleDisconnect() {
    this.status = 'disconnected';

    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.reconnect();
    }, 1000);
  }

  /**
  * Handle a message event
  * @param {MessageEvent} event
  **/
  @bind
  handleMessage(event) {
    const data = JSON.parse(event.data);
    console.log(data);
    this.emit('message', data);
  }

  /**
  * Connect to the server
  **/
  connect() {
    if (this.status == 'connected') throw new Error('Socket is already connected!');

    this.socket = new WebSocket(this.endpoint);
    this.bindEvents();

    this.status = 'connecting';
  }

  /**
  * Reconnect to the server
  **/
  reconnect() {
    this.status = 'reconnecting';

    this.unbindEvents();
    this.connect();
  }

  /**
  * Send a message to the server
  * @param {Object} data The data to send
  **/
  send(data) {
    this.socket.send(JSON.stringify(data));
  }

  /**
  * Authenticates the connection
  **/
  authenticate() {
    this.send({
      type: 'login',
      token: this.authToken,
    });
  }

  /**
  * Sets the login token and attempts authentication
  * @param {String} token the authtoken
  **/
  login(token) {
    this.authToken = token;
    if (this.status == 'connected') this.authenticate();
  }
}

export default SocketConnection;
