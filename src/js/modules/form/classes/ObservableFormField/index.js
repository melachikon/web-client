import {observable, computed} from 'mobx';

/** The observable form fields handles state of a form field **/
export default class ObservableFormField {
  @observable value = null;

  @observable isTouched = false;
  @observable isDirty = false;
  @observable isDisabled = false;

  @observable isValid = false;
  @observable isValidating = false;
  @observable willRevalidate = false;
  @observable validationErrors = [];

  /**
  * Construct
  * @constructor
  * @param {Object} options
  **/
  constructor(options = {}) {
    this.value = options.value || null;
    this.originalValue = options.value || null;

    this.validators = options.validators || [];
    this.modifiers = options.modifiers || [];

    this.config = options.config || {};
  }

  /**
  * Set the value of the field and run any validators
  * @param {Any} value
  **/
  setValue(value) {
    this.value = this.modify(value);

    // Keep validation fresh
    if (this.isValidating) {
      this.willRevalidate = true;
    } else {
      this.validate();
    }
  }

  /**
  * Get the value (override this method for special occasions)
  * @returns {Any} value
  **/
  getValue() {
    return this.value;
  }

  /**
  * Get the serialized value (used when submitting and stuff)
  * @returns {Any} value
  **/
  getSerializedValue() {
    return this.value;
  }

  /**
  * Run modifiers and return the value from it
  * @returns {Any} value
  **/
  modify(value) {
    const modifiers = this.modifiers;

    modifiers.forEach(func => {
      value = func(this.value, value);
    });

    return value;
  }

  /**
  * Validate the field
  * @param {Object} fields object containing fields this field may be together with
  **/
  validate(fields) {
    const validators = this.validators;

    this.isValidating = true;
    this.isValid = true;
    this.validationErrors = [];

    const promises = validators.map(validator => validator(this.value, fields));

    const checkIfResultIsValid = (result) => {
      return result.isValid;
    }

    const extractErrors = (result) => {
      return result.error;
    }

    return this.validationPromise = new Promise((resolve, reject) => {
      Promise.all(promises).then(results => {
        const isValid = this.isValid = results.every(checkIfResultIsValid);
        if (!isValid) this.validationErrors = results.map(extractErrors);
      })
      .catch(reject)
      .then(() => {
        this.isValidating = false;

        // If the input value was changed while validating, validate again after completion
        if (this.willRevalidate) {
          this.willRevalidate = false;
          return this.validate(fields);
        }

        resolve(this.isValid);
      });
    });
  }

  @computed get isReady() {
    return true;
  }
}
