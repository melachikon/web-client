import preact from 'preact';

import InfiniteScrollList from 'common/components/InfiniteScrollList';

const wrapInListHandler = (Component) => {
  class ListHandler extends preact.Component {
    constructor(props) {
      super(props);

      this.renderList = this.renderList.bind(this);
    }

    renderList(items) {
      return (
        <Component items={items} {...this.props}/>
      )
    }

    render() {
      const
      {resourceList} = this.props;

      return (
        <InfiniteScrollList resourceList={resourceList} renderList={this.renderList}/>
      )
    }
  }

  return ListHandler;
}

export default wrapInListHandler;
