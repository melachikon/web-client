import preact from 'preact';
import classNames from 'classnames';
import {observer, inject} from 'mobx-react';

// Components
import ResourceImage from 'common/components/ResourceImage';
import Icon from 'common/components/Icon';

// HOC
import injectCurrentUser from 'modules/auth/hoc/injectCurrentUser';

// Views
import LinkView from './HamburgerNav__LinkView';
import AuthView from './HamburgerNav__AuthView';
import NotifactionView from './HamburgerNav__NotificationsView';
import MessageView from './HamburgerNav__NotificationsView';
import UserOptionsView from './HamburgerNav__UserOptionsView';

@injectCurrentUser
export default class HamburgerNav extends preact.Component {
  constructor() {
    super();
    this.state = {
      'isOpen': false,
      'currentTab': 'navigation',
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleToggle() {
    this.setState({
      'isOpen': !this.state.isOpen,
      'currentTab': 'navigation',
    });
  }

  handleClose() {
    this.setState({
      'isOpen': false
    });
  }

  getTabContentComponent() {
    const
    pathToComponentMap = {
      'navigation': LinkView,
      'authenticate': AuthView,
      'notifications': NotifactionView,
      'messages': MessageView,
      'userOptions': UserOptionsView,
    };

    return pathToComponentMap[this.state.currentTab] || React.Component;
  }

  renderTabTitle() {
    const
    pathToTitleMap = {
      'navigation': 'Menu',
      'notifications': 'Notifications',
      'messages': 'Messages',
      'userOptions': 'User menu',
      'authenticate': 'Account'
    };

    return pathToTitleMap[this.state.currentTab] || 'Undefined tab';
  }

  renderTabButton(icon, path) {
    let
    tabClassName = classNames('masthead__panelTab', {
      'isSelected': this.state.currentTab == path
    }),
    onClick = () => {
      this.setState({'currentTab': path});
    };

    return (
      <div onClick={onClick} key={path} className={tabClassName}>
        <div className="masthead__panelTabContent">
          <icon>{icon}</icon>
        </div>
      </div>
    );
  }

  renderTabs() {
    let
    renderedTabs = [];
    renderedTabs.push(
      this.renderTabButton('app.hamburger', 'navigation')
    );

    if (this.props.authUser) {
      const UserAvatar = (
        <div className="masthead__userAvatar">
          <ResourceImage height="100%" width="100%" resource={this.props.authUser} name="avatar" size="small"/>
        </div>
      );

      renderedTabs.push(
        this.renderTabButton('app.bell', 'notifications'),
        this.renderTabButton('app.message', 'messages'),
        this.renderTabButton(UserAvatar, 'userOptions')
      );
    }

    if (!this.props.authUser) {
      renderedTabs.push(
        this.renderTabButton('app.account', 'authenticate')
      );
    }

    return renderedTabs;
  }

  render() {
    const
    rootDivClasses = classNames("masthead__hamburgerNav", {
      'isOpen': this.state.isOpen
    }),
    TabContent = this.getTabContentComponent();

    return (
        <nav className={rootDivClasses}>
          <a onClick={this.handleToggle} className="wd-button masthead__navButton masthead__hamburgerButton">
            <Icon name="app.hamburger"/>
          </a>
          <div onClick={this.handleClose} className="masthead__navOverlay"></div>
          <div className="masthead__navPanel">
            <div className="masthead__panelHeader">
              <div onClick={this.handleClose} className="masthead__panelBackButton masthead__navButton">
                <Icon name="app.close"/>
              </div>
              <div className="masthead__panelTitle">{this.renderTabTitle()}</div>
            </div>
            <div className="masthead__panelBody">
              <TabContent onClose={this.handleClose}/>
            </div>
            <div className="masthead__panelFooter">
              {this.renderTabs()}
            </div>
          </div>
        </nav>
    )
  }
}
