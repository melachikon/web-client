import preact from 'preact';
import {observer, inject} from 'mobx-react';

// Components
import ResourceImage from 'common/components/ResourceImage';
import Analyser from 'modules/player/components/Analyser';
import PlayButton from 'modules/player/components/PlayButton';
import Seeker from 'modules/player/components/Seeker';

@observer
export default class TrackHero extends preact.Component {
  renderBackground(resource) {
    const
    hasBackgroundArt = resource.media.bgartwork.exists;

    if (!hasBackgroundArt) {
      const
      artworkImageProps = {
        'width': '500px',
        'height': '500px',
        'resource': resource,
        'name': 'artwork',
        'size': 'normal',
        'blur': '50',
      };

      return (
        <div key="pro-artwork" className="trackView__blurArtwork">
          <ResourceImage {...artworkImageProps}/>
        </div>
      )
    }

    if (hasBackgroundArt) {
      const
      artworkImageProps = {
        'resource': resource,
        'name': 'bgartwork',
        'size': 'normal',
        'scaling': 'cover',
      };

      return (
        <div key="artwork" className="trackView__backgroundArtwork">
          <ResourceImage {...artworkImageProps}/>
          <div className="trackView__backgroundFade"/>
        </div>
      )
    }
  }

  renderArtwork(resource) {
    const
    hasBackgroundArt = resource.media.bgartwork.exists;

    if (!hasBackgroundArt) {
      const
      artworkImageProps = {
        'width': '100%',
        'height': '100%',
        'resource': resource,
        'name': 'artwork',
        'size': 'normal',
      };

      return (
        <ResourceImage {...artworkImageProps}/>
      )
    }
  }

  render() {
    const
    {resource} = this.props,
    {data} = resource;

    return (
      <div className="trackView__hero">
        <div className="trackView__background">
          {this.renderBackground(resource)}
        </div>
        <div className="trackView__foreground isOverlay">
          <div className="trackView__artwork">
            {this.renderArtwork(resource)}
          </div>
          <div className="trackView__heroMain">
            <div className="trackView__heroHeader">
              <div className="trackView__playButton">
                <PlayButton className="playButton playButton--big" resource={resource}/>
              </div>
              <div className="trackView__primaryInfo">
                <div className="trackView__title">{data.title}</div>
                <div className="trackView__type">{data.type}</div>
              </div>
              <div className="trackView__genreBadge">{data.genre}</div>
            </div>
            <div className="trackView__heroBody">
              <div className="trackView__analyser">
                <Analyser/>
              </div>
            </div>
            <div className="trackView__heroFooter">
              <Seeker resource={resource}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
