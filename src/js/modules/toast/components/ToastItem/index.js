import preact from 'preact'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'

@observer
export default class ToastItem extends preact.Component {
  static defaultProps = {
    duration: 3000,
    onTimeout: () => {},
    onClick: () => {},
  }

  @observable timeRemaining = this.props.duration

  // the two properties below might need better names, perhaps use a state machine?

  /** Whether the component's internal timer is running */
  @observable running = false

  /** Whether or not the internal timer decrements timeRemaining */
  @observable paused = false

  @action
  runAnimation = () => {
    this.running = true

    let currentTime = window.performance.now()
    const runFrame = () => {
      const now = window.performance.now()
      const elapsed = now - currentTime
      currentTime = now

      if (this.running) {
        requestAnimationFrame(runFrame)
      }
      if (!this.paused) {
        this.timeRemaining -= elapsed
      }

      if (this.timeRemaining <= 0) {
        this.props.onTimeout()
        this.stopAnimation()
      }
    }
    requestAnimationFrame(runFrame)
  }

  @action
  stopAnimation = () => {
    this.running = false
  }

  @action
  setPaused = paused => {
    this.paused = paused
  }

  componentDidMount() {
    this.runAnimation()
  }

  componentWillUnmount() {
    this.stopAnimation()
  }

  render() {
    const scale = this.timeRemaining / this.props.duration
    return (
      <div
        class="ToastItem"
        onClick={this.props.onClick}
        onMouseEnter={() => this.setPaused(true)}
        onMouseLeave={() => this.setPaused(false)}
      >
        <div class="body">{this.props.children}</div>
        <div class="timeoutBar" style={{ transform: `scaleX(${scale})` }} />
      </div>
    )
  }
}
