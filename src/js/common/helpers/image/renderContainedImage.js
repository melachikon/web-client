export function renderContainedImage(image, context) {
  // Borrowed code from: https://sdqali.in/blog/2013/10/03/fitting-an-image-in-to-a-canvas-object/
  let
  canvas = context.canvas,
  imageAspectRatio = image.width / image.height,
  canvasAspectRatio = canvas.width / canvas.height,
  renderableHeight, renderableWidth, xStart, yStart;

  if (imageAspectRatio < canvasAspectRatio) {
    renderableHeight = canvas.height;
    renderableWidth = image.width * (renderableHeight / image.height);
    xStart = (canvas.width - renderableWidth) / 2;
    yStart = 0;
  }

  else if (imageAspectRatio > canvasAspectRatio) {
    renderableWidth = canvas.width
    renderableHeight = image.height * (renderableWidth / image.width);
    xStart = 0;
    yStart = (canvas.height - renderableHeight) / 2;
  }

  else {
    renderableHeight = canvas.height;
    renderableWidth = canvas.width;
    xStart = 0;
    yStart = 0;
  }

  context.drawImage(image, xStart, yStart, renderableWidth, renderableHeight);
  return context.getImageData(0, 0, renderableWidth, renderableHeight);
}
