import injectPlayer from './injectPlayer';
import injectQueue from './injectQueue';

export {injectPlayer, injectQueue};
