import preact from 'preact';
import {observer, inject} from 'mobx-react';

export default function injectCurrentUser(Component) {
  @inject('AuthStore') @observer
  class AuthUserDataWrapper extends preact.Component {
    render() {
      return <Component {...this.props} authUser={this.props.AuthStore.currentUser}/>
    }
  }

  return AuthUserDataWrapper;
}
