// The analyser node group is an advanced wrapper for the AnalyserNode
// and supports stereo visualization

export default class AnalyserNodeGroup {
  constructor(context) {
    this.context = context;

    this.channels = {
      'left': context.createAnalyser(),
      'right': context.createAnalyser()
    };

    this.channels.left.smoothingTimeConstant = 0.7;
    this.channels.right.smoothingTimeConstant = 0.7;

    this.splitter = context.createChannelSplitter(2);
    this.merger = context.createChannelMerger(2);

    this.connectNodes();
  }

  connectNodes() {
    // Connect splitter to analysers
    this.splitter.connect(this.channels.left, 0);
    this.splitter.connect(this.channels.right, 1);

    // Connect analysers to merger
    this.channels.left.connect(this.merger, 0, 0);
    this.channels.right.connect(this.merger, 0, 1);
  }

  connectInput(node) {
    node.connect(this.splitter);
  }

  connectOutput(node) {
    this.merger.connect(node);
  }
}
