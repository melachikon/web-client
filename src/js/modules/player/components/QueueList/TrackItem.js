import preact from 'preact'
import bem from 'bem-classname'
import { observer, inject } from 'mobx-react'
import ResourceImage from 'common/components/ResourceImage'
import { wrapInLazyLoadHandler } from 'common/hoc'
import Icon from 'common/components/Icon'
import { bind } from 'decko'

const classNames = bem.bind(null, 'queueTrackItem')

const renderPlaceholder = () => {
  return (
    <div className="queueTrackItem__placeholder">
      <div className="queueTrackItem__placeholderArtwork" />
      <div className="queueTrackItem__placeholderBody" />
    </div>
  )
}

@wrapInLazyLoadHandler(renderPlaceholder)
@inject('PlayerStore')
@observer
export default class TrackItem extends preact.Component {
  @bind
  handleSelect() {
    const { PlayerStore, index, queueIndex } = this.props
    PlayerStore.goto(queueIndex, index)
  }

  @bind
  handleTrackRemoved(event) {
    event.preventDefault()
    event.stopPropagation()
    // remove track from playlist
  }

  render() {
    const { resource, isActive } = this.props
    return (
      <div className={classNames({ isActive })}>
        <div style={{ flexShrink: 0, paddingRight: '1rem' }} onClick={this.handleTrackRemoved}>
          <Icon name="app.close" />
        </div>
        <div className={classNames('artwork')} onClick={this.handleSelect}>
          <ResourceImage resource={resource} cheap={true} name="artwork" size="small" />
        </div>
        <div className={classNames('content')} onClick={this.handleSelect}>
          <div className={classNames('title')}>{resource.data.title}</div>
          <div className={classNames('author')}>{resource.user.data.display_name}</div>
        </div>
      </div>
    )
  }
}
