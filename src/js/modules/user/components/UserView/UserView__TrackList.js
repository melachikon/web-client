import preact from 'preact';

// Components
import TrackList from 'modules/track/components/TrackList';

export default class UserTrackList extends preact.Component {
  render() {
    const {resource} = this.props;

    const queueName = `${resource.data.displayName}'s tracks`;

    return (
      <TrackList queueName={queueName} resourceList={resource.tracks}/>
    )
  }
}
