import {observable} from 'mobx';

class AuthStore {
  @observable currentUser = null;
}

export default new AuthStore;
