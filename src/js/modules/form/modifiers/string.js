export function allowCharacters(regex) {
  return function(oldValue, newValue) {
    let allowed = new RegExp('^[' + regex + ']+$|(^$)', 'g')

    if (newValue.match(allowed)) {
      return newValue
    }

    return oldValue
  }
}

export function replaceAll(char, repl) {
  return function(oldValue, newValue) {
    return newValue.replace(new RegExp(char, 'g'), repl);
  }
}

export function setCase(type) {
  return function(oldValue, newValue) {
    switch(type) {
      case 'lower': {
        return newValue.toLowerCase()
        break;
      }
      case 'upper': {
        return newValue.toUpperCase()
        break;
      }
      default: {
        return newValue
      }
    }
  }
}
