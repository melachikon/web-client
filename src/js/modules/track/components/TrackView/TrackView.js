import preact from 'preact';
import {observer, inject} from 'mobx-react';
import classNames from 'classnames';
import {injectResource} from 'common/hoc/';
import {NavLink} from 'react-router-dom';

// HOC
import {injectPlayer} from 'modules/player/hoc/';

// Components
import ResourceImage from 'common/components/ResourceImage';

// Related components
import TrackHero from './TrackHero';

@injectResource('TrackStore', (props) => {
  return props.TrackStore.fetchItemByReference(props.match.params.username + '_' + props.match.params.slug);
})
@injectPlayer
@observer
export default class TrackView extends preact.Component {
  render() {
    const
    {resource, player} = this.props,
    {data} = resource;

    const
    rootDivClassname = classNames('trackView', {
      'isActive': resource.isActive,
      'isPlaying': resource.isPlaying,
      'isPaused': !resource.isPlaying,
      'isBuffering': player.isBuffering,
      'hasBgArt': resource.media.bgartwork.exists,
    });

    return (
      <div className={rootDivClassname}>
        <TrackHero player={player} {...this.props}/>
        <NavLink to={`/@${resource.data.user.username}`}>User</NavLink>
      </div>
    )
  }
}
