import preact from 'preact';
import {observer, inject} from 'mobx-react';

import {traverseElement} from 'common/helpers/element';

@inject('PopoverStore') @observer
export default class FixedPopoverTrigger extends preact.Component {
  constructor(props) {
    super(props);

    this.state = {
      'popoverKey': null,
      'popoverRef': null,
      'isOpen': false,
    };

    this.PopoverStore = props.PopoverStore;

    this.handleOutsideClick = this.handleOutsideClick.bind(this);
    this.getRef = this.getRef.bind(this);
  }

  showPopover() {
    const onOpen = (ref, key) => {
      this.setState({
        'popoverKey': key,
        'popoverRef': ref,
        'isOpen': true
      });
    };

    const onClose = () => {
      this.setState({
        'popoverKey': null,
        'popoverRef': null,
        'isOpen': false,
      });
    };

    const options = {
      'type': 'element',
      'element': this.triggerRef,
      'preferredAlignment': this.props.preferredAlignment || {},
      'fixed': this.props.isFixed || false,
    };

    this.PopoverStore.addPopover(
      this.props.renderPopoverContent,
      options,
      onOpen,
      onClose
    );
  }

  componentDidMount() {
    window.addEventListener('click', this.handleOutsideClick, true);
  }

  componentWillUnmount() {
    if (this.state.isOpen) this.dismissPopover();

    window.removeEventListener('click', this.handleOutsideClick, true);
  }

  dismissPopover() {
    this.PopoverStore.dismissPopover(this.state.popoverKey);
  }

  togglePopover() {
    if (this.state.isOpen)
    return this.dismissPopover();
    return this.showPopover();
  }

  handleOutsideClick(event) {
    let
    shouldDismiss = true,
    shouldToggle = false;

    traverseElement(event.target, (element) => {
      if (element == this.state.popoverRef) {
        shouldDismiss = false;
      };
      if (element == this.triggerRef) {
        shouldToggle = true;
      }
    })

    if (shouldToggle) return this.togglePopover();
    if (shouldDismiss) return this.dismissPopover();
  }

  getRef(ref) {
    this.triggerRef = ref;
  }

  render() {
    return (
      <div ref={this.getRef} data-fixed-popover-trigger>
        {this.props.children}
      </div>
    )
  }
}
