import {observable, computed, action} from 'mobx';
import {ApiServer} from 'network/http';

import ResourceStore from './ResourceStore';

export default class ReferencedResourceStore extends ResourceStore {
  @observable references = {};

  getDatabaseSchema() {
    return {
      key: {keyPath: 'id'},
      indexes: {
        referenceName: {unique: true},
        dateCreated: {},
        dateModified: {},
        data: {},
      }
    };
  }

  getReferenceName(data) {
    return data.id;
  }

  getReference(ref) {
    return this.references[ref] || ref;
  }

  @action setReference(ref, id) {
    this.references[ref] = id;
  }

  @action getItemByReference(ref) {
    return this.items[this.getReference(ref)];
  }

  @action addOrUpdateItemInDatabase(data, item, referenceName) {
    if (this.hasDatabaseTable) {
      this.databaseTable.put({
        id: data.id,
        referenceName: referenceName,
        dateCreated: data.date_created,
        dateModified: data.date_modified,
        data: data
      })
      .then(() => {
        item.isInDatabase = true;
      })
    }
  }

  @action addItem(data, item) {
    let
    referenceName = this.getReferenceName(data);
    item = item || this.getOrCreateItem(data.id);

    // Reference
    this.setReference(referenceName, data.id);

    // Rename
    if (this.items[referenceName]) {
      this.renameItem(referenceName, data.id);
    }

    // Item updating
    item.setData(data);

    item.isFetched = true;
    item.fetchStatus = 'fulfilled';

    this.addOrUpdateItemInDatabase(data, item, referenceName)
    return item
  }

  // Overridable
  getReferenceUrl(ref) {
    return this.endpoint + '/' + ref + '/';
  }

  @action fetchItemByReference(ref) {
    let item = this.getOrCreateItem(this.getReference(ref));
    this.setReference(ref, this.getReference(ref));

    // Fetches by reference
    if (this.hasDatabaseTable) {
      this.databaseTable.query('referenceName')
      .only(ref)
      .distinct()
      .execute()
      .then((result) => {
        const dbItem = result[0];
        if (dbItem) this.addItem(dbItem.data, item);
      });
    }

    ApiServer.GET(this.getReferenceUrl(ref))
    .then(response => {
      this.addItem(response.data, item)
    })

    return item;
  }
}
