import {MEDIA_URL} from 'network/urls';

export default function(token, bitrate) {
  return `http://${MEDIA_URL}/images/${token}`;
}
