import {observable, computed} from 'mobx';

/** The observable form handles form management **/
export default class ObservableForm {
  @observable isDisabled = false;
  @observable isValidating = false;

  constructor() {
    this.fields = {};

    this.registerFields();
  }

  /**
  * Get fields to register in form
  * @returns {Object} object containing fields
  **/
  getFields() {
    return {};
  }

  /**
  * Register the fields
  **/
  registerFields() {
    const fields = this.getFields();

    Object.keys(fields).forEach((fieldKey) => {
      const field = fields[fieldKey];

      this.fields[fieldKey] = new field.class(field.options);
    });
  }

  /**
  * Validate the fields
  * @returns {Promise}
  **/
  validate() {
    const promises = Object.keys(this.fields).map(key => this.fields[key].validate());

    return new Promise((resolve, reject) => {
      let isValid = true;

      Promise.all(promises).then((results) => {
        isValid = results.every(x => (x == true));

        resolve(isValid);
      }).catch(reject);
    });
  }

  /**
  * Get the serialized data to use when submitting
  * @returns {Object} data
  **/
  getSerializedData() {
    const result = {};

    Object.keys(this.fields).forEach(key => {
      result[key] = this.fields[key].getSerializedValue();
    });

    return result;
  }

  @computed get isReady() {
    return Object.keys(this.fields).every(key => {
      return this.fields[key].isReady;
    });
  }
}
