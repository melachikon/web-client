import {API_URL, MEDIA_URL} from 'network/urls';
import RequestHandler from './classes/RequestHandler';

// Request handlers for each server

const ApiServer = new RequestHandler(API_URL, {}, 'http'),
      MediaServer = new RequestHandler(MEDIA_URL, {}, 'http');

export {ApiServer, MediaServer};
