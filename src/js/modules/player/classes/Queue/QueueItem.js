import {observable, action, computed} from 'mobx';

import {TrackStore} from 'modules/track/stores';
import {ResourceList} from 'common/classes/List';

import moment from "moment";

/**
  The queue item is part of the queue, except it handles indexes and all that by itself,
  and tells the queue when it should move on.
**/

export default class QueueItem {
  @observable name = 'Unnamed queue item';
  @observable type = 'auto';

  @observable tracks = null;
  @observable index = 0;

  @observable fetchTrigger = 30;

  constructor(name, endpoint, data = {}) {
    this.id = performance.now();
    this.name = name;
    this.endpoint = endpoint;

    this.clientLink = data.clientLink;
    this.type = data.type || 'auto';

    this.tracks = new ResourceList(endpoint, TrackStore);

    Object.assign(this.tracks, {
      'offset': data.offset || 0,
      'limit': 50,
      'filters':  data.filters || {},
      'timestamp': data.timestamp || this.tracks.timestamp,
    });

    this.startingOffset = this.tracks.offset;
    this.index = data.index || 0;

    this.fetchItems();
  }

  @bind
  @action
  update(data) {
    const shouldClear = (
      data.timestamp !== this.tracks.timestamp
    );

    if (shouldClear) {
       this.refresh(data);
       return;
    }

    const newTrack = this.gotoTrack(data.track.data.id);

    if (!newTrack) {
      this.refresh(data);
      this.index = 0;
    }
  }

  @bind
  @action
  refresh(data) {
    this.tracks.timestamp = data.timestamp;
    this.tracks.clear();

    this.tracks.offset = data.offset;
    this.startingOffset = data.offset;

    this.fetchIfNecessary();
  }

  @bind
  @action
  fetchItems() {
    this.fetch()
    .then(() => {
      this.fetchIfNecessary()
    });
  }

  @bind
  @action
  fetchIfNecessary() {
    const
    shouldFetch = (
      this.status !== 'pending' &&
      this.indexFromEnd < this.fetchTrigger &&
      this.hasMore
    );

    if (shouldFetch) this.fetchItems();
  }

  @action
  setIndex(index) {
    const
    newIndex = Math.min(Math.max(parseInt(index), 0), this.items.length);

    this.index = newIndex;

    this.fetchIfNecessary();
    return index;
  }

  @action
  goNext() {
    const
    nextIndex = this.index + 1,
    hasMore = this.indexFromEnd > 0;

    if (hasMore) {
      this.index = nextIndex;
      this.fetchIfNecessary();
      return this.currentItem;
    }

    return null;
  }

  @action
  goPrevious() {
    const
    previousIndex = this.index - 1;

    if (previousIndex >= 0) {
      this.index = previousIndex;
      this.fetchIfNecessary();
      return this.currentItem;
    }

    return null;
  }

  @action
  gotoTrack(id) {
    return this.tracks.items.find((track, index) => {
      if (track.data.id == id) {
        this.index = index;
        this.fetchIfNecessary();

        return true;
      }
    });
  }

  @computed
  get currentItem() {
    return this.items[this.index];
  }

  @computed
  get indexFromEnd() {
    return Math.abs((this.items.length - 1) - this.index);
  }

  @computed
  get localStorageData() {
    return {
      'name': this.name,
      'endpoint': this.endpoint,
      'data': {
        'clientLink': this.clientLink,
        'offset': this.startingOffset + this.index,
        'type': this.type,
        'timestamp': this.timestamp,
      }
    };
  }

  // Aliasing
  get status() {
    return this.tracks.status;
  }

  get fetch() {
    return this.tracks.fetch;
  }

  get offset() {
    return this.tracks.offset;
  }

  get limit() {
    return this.tracks.limit;
  }

  get timestamp() {
    return this.tracks.timestamp;
  }

  get hasMore() {
    return this.tracks.hasMore;
  }

  get hasInitial() {
    return this.tracks.hasInitial;
  }

  get items() {
    return this.tracks.items;
  }

  get count() {
    return this.tracks.count;
  }
}
