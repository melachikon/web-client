import preact from 'preact';
import classNames from 'classnames';
import {observer, inject} from 'mobx-react';

// Helpers
import {convertSecondsToHHMMSS} from 'modules/player/helpers/convertTime';

// Components
import SliderInput from 'common/components/SliderInput';

// Player
import Player from 'player';

@observer
export default class Seeker extends preact.Component {
  constructor(props) {
    super(props);

    this.isSeeking = false;
    this.isAnimating = true;

    this.performFrame = this.performFrame.bind(this);
    this.getCurrentTimeRef = this.getCurrentTimeRef.bind(this);
    this.getSliderRef = this.getSliderRef.bind(this);

    this.handleGrab = this.handleGrab.bind(this);
    this.handleRelease = this.handleRelease.bind(this);
    this.handleMove = this.handleMove.bind(this);
  }

  // Frames
  performFrame() {
    // "Good code is sentence" ~ Mir, 2017
    const {resource} = this.props;
    const {currentTime, duration} = resource;

    const currentProgress = (currentTime / duration);

    this.seekerSlider.setValue(currentProgress);
    this.currentTimeElement.innerText = convertSecondsToHHMMSS(currentTime);

    requestAnimationFrame(this.shouldAnimate);
  }

  setDisplayTime(value) {
    const {resource} = this.props,
          currentTime = (value * resource.duration);

    this.currentTimeElement.innerText = convertSecondsToHHMMSS(currentTime);
  }

  // Event handler
  handleGrab(value) {
    this.isSeeking = true;

    this.setDisplayTime(value);
  }

  handleRelease(value) {
    const {resource} = this.props;

    Player.setTime(value * resource.duration);

    this.isSeeking = false;
    this.shouldAnimate();
  }

  handleMove(value) {
    this.setDisplayTime(value);
  }

  // Refs
  getCurrentTimeRef(ref) {
    this.currentTimeElement = ref;
  }

  getSliderRef(ref) {
    this.seekerSlider = ref;
  }

  // Lifecycle
  componentDidUpdate(props) {
    this.shouldAnimate();
  }

  componentDidMount() {
    this.shouldAnimate();
  }

  componentWillUnmount() {
    this.isAnimating = false;
  }

  @bind
  shouldAnimate() {
    const {resource} = this.props;

    const willAnimate = (
      resource.isPlaying
      && this.isAnimating
      && !this.isSeeking
    );

    if (willAnimate) {
      this.performFrame();
    }
  }

  render() {
    const {resource} = this.props;

    /** Trick the component to update when told to **/
    resource.isPlaying;

    const rootDivClassnames = classNames('playerSeeker', {
      'isActive': resource.isActive
    });

    const sliderInputProps = {
      onGrab: this.handleGrab,
      onRelease: this.handleRelease,
      onMove: this.handleMove,
    };

    return (
      <div className={rootDivClassnames}>
        <div ref={this.getCurrentTimeRef} className="playerSeeker__currentTime">{convertSecondsToHHMMSS(resource.currentTime)}</div>
        <div className="playerSeeker__sliderInput">
          <SliderInput ref={this.getSliderRef} {...sliderInputProps}/>
        </div>
        <div className="playerSeeker__duration">{convertSecondsToHHMMSS(resource.data.duration)}</div>
      </div>
    )
  }
}
