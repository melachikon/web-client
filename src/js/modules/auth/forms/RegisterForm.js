import ObservableForm from 'modules/form/classes/ObservableForm';
import ObservableFormField from 'modules/form/classes/ObservableFormField';

// Modifiers
import * as stringModifiers from 'modules/form/modifiers/string';

// Validators
import * as stringValidators from 'modules/form/validators/string';

export default class RegisterForm extends ObservableForm {
  getFields() {
    const username = {
      'class': ObservableFormField,
      'options': {
        'modifiers': [
          stringModifiers.setCase('lower'),
          stringModifiers.replaceAll(' ', '-'),
          stringModifiers.allowCharacters('a-z0-9\-'),
        ],
        'validators': [
          stringValidators.isRequired()
        ]
      },
    }

    const email = {
      'class': ObservableFormField,
      'options': {
        'validators': [
          stringValidators.isRequired()
        ]
      },
    }

    const password = {
      'class': ObservableFormField,
      'options': {
        'validators': [
          stringValidators.isRequired()
        ]
      },
    }

    const cpassword = {
      'class': ObservableFormField,
      'options': {
        'validators': [
          stringValidators.isRequired()
        ]
      },
    }

    const captcha = {
      'class': ObservableFormField,
      'options': {
        'validators': [
          stringValidators.isRequired('Please verify captcha')
        ]
      },
    }

    return {username, email, password, cpassword, captcha};
  }
}
