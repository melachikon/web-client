import preact from 'preact';
import getClassName from 'classnames';

import {ApiServer} from 'network/http';

// Actions
import {authenticate} from 'modules/auth/actions';

// Modal components
import ModalHeader from 'modules/modal/components/ModalItem/Header';
import ModalBody from 'modules/modal/components/ModalItem/Body';
import ModalFooter from 'modules/modal/components/ModalItem/Footer';

// Components
import Button from 'common/components/Button';
import FormContext from 'modules/form/components/FormContext';
import FormField from 'modules/form/components/FormField';

// Inputs
import TextInput from 'modules/form/components/TextInput';
import Captcha from 'modules/auth/components/Captcha';

// Forms
import RegisterForm from 'modules/auth/forms/RegisterForm';

export default class RegisterModal extends preact.Component {
  constructor() {
    super();

    this.form = new RegisterForm();

    window.test = this;
  }

  /** REFACTOR THIS **/

  @bind
  handleSubmit() {
    this.form.validate().then((isValid) => {
      const data = this.form.getSerializedData();

      if (isValid) return ApiServer.POST('/auth/register', data);
    })
    .then((response) => {
      const data = this.form.getSerializedData();

      return ApiServer.POST('/auth/login/local', {
        'username': data.username,
        'password': data.password,
      });
    })
    .then((response) => {
      localStorage.authToken = response.data.token;
      return authenticate();
    })
    .then((response) => {
      this.props.dismiss();
    });
  }

  render() {
    return (
      <div className="RegisterModal">
        <ModalHeader title="Register" subtitle="Create your account"/>
        <ModalBody>
          <FormContext form={this.form}>
            <div className="FormGrid">
              <FormField name="username" label="Username" size="full">
                <TextInput/>
              </FormField>
              <FormField name="email" label="E-mail" size="full">
                <TextInput/>
              </FormField>
              <FormField name="password" label="Password" size="full">
                <TextInput type="password"/>
              </FormField>
              <FormField name="cpassword" label="Confirm password" size="full">
                <TextInput type="password"/>
              </FormField>
              <FormField name="captcha" label="Captcha" size="full">
                <Captcha/>
              </FormField>
            </div>
          </FormContext>
          <p className="Paragraph">{'Disclaimer: Since WaveDistrict is still in its development phase, your account is not guaranteed to survive on the release date.'}</p>
        </ModalBody>
        <ModalFooter>
          <Button onClick={this.handleSubmit} label="Submit"/>
          <Button onClick={this.props.dismiss} label="Cancel"/>
        </ModalFooter>
      </div>
    )
  }
}
