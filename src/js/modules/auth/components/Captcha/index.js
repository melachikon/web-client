import preact from 'preact';

export default class Captcha extends preact.Component {
  constructor(props) {
    super(props);

    this.controller = props.controller;
  }

  componentWillMount() {
    this.captchaId = window.performance.now();
  }

  componentDidMount() {
    this.widgetId = window.grecaptcha.render(this.container, {
      'sitekey' : '6LdICysUAAAAAEQn2ekzTotfH82OU5RmiGFiQlHl',
      'callback' : this.handleVerifiy,
      'theme': 'white',
    });
  }

  shouldComponentUpdate() {
    return false;
  }

  @bind
  handleVerifiy(data) {
    this.controller.setValue(data);
  }

  @bind
  getCaptchaContainer(ref) {
    this.container = ref;
  }

  componentWillUnmount() {
    console.log(this.widgetId);
    window.grecaptcha.reset();
  }

  render() {
    return (
      <div className="auth-captcha">
        <div key={Math.round(window.performance.now())} ref={this.getCaptchaContainer} id={`g-captcha`}/>
      </div>
    );
  }
}
