import ObservableForm from 'modules/form/classes/ObservableForm';
import ObservableFormField from 'modules/form/classes/ObservableFormField';

// Fields
import FileField from 'modules/form/classes/ObservableFormField/FileField';
import ImageField from 'modules/form/classes/ObservableFormField/ImageField';

// Modifiers
import * as stringModifiers from 'modules/form/modifiers/string';

// Validators
import * as stringValidators from 'modules/form/validators/string';

export default class TrackUploadForm extends ObservableForm {
  getFields() {
    const audio = {
      'class': FileField,
      'options': {
        'accept': ['audio/mpeg', 'audio/mp3'],
        'uploadTo': '/audio',
        'validators': [
          stringValidators.isRequired()
        ]
      }
    }

    const artwork = {
      'class': ImageField,
      'options': {
        'accept': ['image/png', 'image/jpeg'],
        'uploadTo': '/image?type=artwork'
      }
    }

    const title = {
      'class': ObservableFormField,
      'options': {
        'validators': [
          stringValidators.isRequired()
        ]
      },
    }

    const slug = {
      'class': ObservableFormField,
      'options': {
        'modifiers': [
          stringModifiers.setCase('lower'),
          stringModifiers.replaceAll(' ', '-'),
          stringModifiers.allowCharacters('a-z0-9\-'),
        ],
        'validators': [
          stringValidators.isRequired()
        ]
      },
    }

    const description = {
      'class': ObservableFormField,
    }

    return {audio, artwork, title, slug, description};
  }
}
