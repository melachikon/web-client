import preact from 'preact';
import {BrowserRouter, Route} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory'

// Components
import ApplicationView from 'modules/core/components/ApplicationView'

// Import routes
import ApplicationRoutes from 'modules/core/routes';

const history = createHistory();
export default class RouteProvider extends preact.Component {
  render() {
    return (
      <BrowserRouter history={history}>
        <ApplicationView>
          <ApplicationRoutes/>
        </ApplicationView>
      </BrowserRouter>
    )
  }
}
