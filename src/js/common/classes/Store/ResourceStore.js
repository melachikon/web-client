import {observable, computed, action} from 'mobx';
import {ApiServer} from 'network/http';

import ModelStore from './ModelStore';
import {ResourceModel} from 'common/classes/Model';

import ResourceStoreDB from 'common/databases/ResourceStoreDB';

export default class ResourceStore extends ModelStore {
  constructor(endpoint, name) {
    super();

    this.name = name;
    this.endpoint = endpoint;

    this.addItem = this.addItem.bind(this);
    this.fetchItem = this.fetchItem.bind(this);

    this.databaseTable = null;
    this.hasDatabaseTable = false;

    //ResourceStoreDB.registerTable(this.name, this.getDatabaseSchema());
    //ResourceStoreDB.addListener(this.name, this.handleGetTable.bind(this));
  }

  handleGetTable(table) {
    this.hasDatabaseTable = true;
    this.databaseTable = table;
  }

  getDatabaseSchema() {
    return {
      key: {keyPath: 'id'},
      indexes: {
        dateCreated: {},
        dateModified: {},
        data: {},
      }
    };
  }

  @action addOrUpdateItemInDatabase(data, item) {
    if (this.hasDatabaseTable) {
      this.databaseTable.put({
        id: data.id,
        dateCreated: data.date_created,
        dateModified: data.date_modified,
        data: data
      })
      .then(() => {
        item.isInDatabase = true;
      })
    }
  }

  @action addItem(data, item) {
    item = item || this.getOrCreateItem(data.id);
    item.setData(data);

    item.isFetched = true;
    item.fetchStatus = 'fulfilled';

    this.addOrUpdateItemInDatabase(data, item);
    return item;
  }

  @action fetchItem(id) {
    let item = this.getOrCreateItem(id);

    if (this.hasDatabaseTable) {
      this.databaseTable.get(id)
      .then(response => {
        if (response) this.addItem(response.data, item);
      })
    }

    ApiServer.GET(this.endpoint + '/' + id + '/')
    .then(response => {this.addItem(response.data, item)})
    .catch(error => {
      item.fetchError = error;
    })

    return item;
  }
}
