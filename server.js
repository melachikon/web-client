const express = require('express')
const app = express()

const publicPath = __dirname + '/build'
const port = 3300

app.use(express.static(publicPath))

app.get('*', function(req, res) {
  res.sendFile(publicPath + '/index.html')
})

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
