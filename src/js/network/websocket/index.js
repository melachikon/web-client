import {API_URL, MEDIA_URL} from 'network/urls';
import SocketConnection from './classes/SocketConnection';

const MediaServerConnection = new SocketConnection(MEDIA_URL);

export {MediaServerConnection}
