export default class BasicAudioSource {
  constructor() {
    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);

    this.audio = new Audio();
  }

  play() {
    return this.audio.play();
  }

  pause() {
    return this.audio.pause();
  }

  setAudioURL(url) {
    if (url !== this.audio.src) this.audio.src = url;
  }

  setVolume(volume) {
    this.audio.volume = volume;
  }

  setTime(time) {
    this.audio.currentTime = time;
  }

  get currentTime() {
    return this.audio.currentTime;
  }

  get duration() {
    return this.audio.duration;
  }
}
