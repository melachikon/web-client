import preact from 'preact';

import ScrollContainer from 'common/components/ScrollContainer';

export default class ModalBody extends preact.Component {
  render() {
    return (
      <ScrollContainer>
        <div className="ModalBody">
          {this.props.children}
        </div>
      </ScrollContainer>
    );
  }
}
