import preact from 'preact';
import classNames from 'classnames';
import {observer, inject} from 'mobx-react';
import {observable} from 'mobx';

import ImageRenderer from './ImageRenderer';

export default class Image extends preact.Component {
  @observable images = [];

  constructor() {
    super();
    this.state = {
      'hasDimensions': false,
      'width': '0px',
      'height': '0px',
    };
  }

  componentDidMount() {
    this.setDimensions();
    window.addEventListener('resize', this.handleResize, false);
  }

  componentWillUnmount() {
    clearInterval(this.resizeTimer);
    window.removeEventListener('resize', this.handleResize, false);
  }

  @bind
  handleResize() {
    clearInterval(this.resizeTimer);

    this.resizeTimer = setTimeout(() => {
      this.setDimensions();
    }, 200);
  }

  setDimensions() {
    this.setState({
      'hasDimensions': true,
      'width': this.container.offsetWidth + 'px',
      'height': this.container.offsetHeight + 'px',
    });
  }

  getContainerRef(container) {
    this.container = container;
  }

  renderCanvases() {
    let
    {preload, main, blur, scaling} = this.props,
    renderedList = [],
    canvasProps = {
      'width': this.state.width || '100%',
      'height': this.state.height || '100%',
    };

    if (preload) {
      renderedList.push(
        <ImageRenderer scaling="cover" className="imageCollection__preloadRenderer" autoSize blurScale={3} key='preload' src={preload} {...canvasProps}/>
      );
    }

    if (main) {
      const
      mainBlurScale = blur || 0;

      renderedList.push(
        <ImageRenderer scaling={scaling} className="imageCollection__mainRenderer" blurScale={mainBlurScale} key='main' src={main} {...canvasProps}/>
      );
    }

    return renderedList;
  }

  render() {
    let
    {props, state} = this,
    // Classes for root div
    rootDivClasses = classNames({
      'imageCollection': true,
      'hasLightbox': !!props.lightbox,
    }),
    rootDivStyle = {
      'width': props.width || '100%',
      'height': props.height || '100%',
    };

    return (
      <div ref={this.getContainerRef.bind(this)} style={rootDivStyle} className={rootDivClasses}>
        {state.hasDimensions && this.renderCanvases()}
      </div>
    )
  }
}
