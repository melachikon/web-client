import preact from 'preact';
import {observer, inject} from 'mobx-react';

const createModalView = (Modal) => {
  @inject('ModalStore') @observer
  class ModalView extends preact.Component {
    componentWillMount() {
      const { router } = this.context;

      console.log(router);
    }

    render() {
      return null;
    }
  }

  return ModalView;
}

export default createModalView;
