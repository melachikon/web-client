import {observable, computed} from 'mobx';
import ObservableFormField from './index';

import FileUpload from 'network/http/classes/FileUpload';

export default class FileField extends ObservableFormField {
  @observable hasMediaId = false;
  @observable mediaId = null;

  constructor(options) {
    super(options);

    this.accept = options.accept || [];

    this.uploader = new FileUpload({
      'uploadTo': options.uploadTo,
    });

    console.log(options);
  }

  validateMime(file) {
    const isValid = this.accept.includes(file.type);

    if (!isValid) alert('Your file is invalid.');

    return isValid;
  }

  setValue(value) {
    const isValid = this.validateMime(value);

    if (isValid) {
      super.setValue(value);

      this.validationPromise.then(isValid => {
        this.uploader.upload(value);
      });
    }
  }

  getSerializedValue() {
    return this.uploader.mediaId;
  }

  @computed get isReady() {
    return (!this.value || !!this.uploader.mediaId);
  }
}
