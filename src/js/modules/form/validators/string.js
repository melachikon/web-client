export function isRequired(errorMsg = 'This field is required') {
  return (value, fields = {}) => new Promise((resolve, reject) => {
    if (!value) return resolve({
      'isValid': false,
      'error': errorMsg,
    });

    resolve({
      'isValid': true,
    });
  });
}
