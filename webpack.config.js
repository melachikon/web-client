const webpack = require('webpack')
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')

const inDevelopment = process.env.NODE_ENV !== 'production'

const sourceDirectory = path.resolve(__dirname, 'src')
const buildDirectory = path.resolve(__dirname, 'build')
const publicDirectory = path.resolve(__dirname, 'public')

if (inDevelopment) {
  console.log('Currently in development mode')
}

/** Sass loader **/
const extractSass = new ExtractTextPlugin({
  filename: 'default.css',
})

function getSassLoader() {
  return {
    test: /\.s[ca]ss$/,
    loader: inDevelopment
      ? 'style-loader!css-loader!sass-loader'
      : extractSass.extract('css-loader!sass-loader'),
  }
}

/** Babel loader **/

function getBabelLoader() {
  const presets = ['es2015', 'stage-0']

  const plugins = [
    'transform-decorators-legacy',
    'transform-class-properties',
    ['transform-react-jsx', { pragma: 'preact.h' }],
  ]

  return {
    test: /\.jsx?$/,
    exclude: /node_modules/,
    loader: 'babel-loader',
    query: { presets, plugins },
  }
}

/** Define plugins **/

function getPlugins() {
  const decko = new webpack.ProvidePlugin({
    bind: ['decko', 'bind'],
  })

  const uglify = new webpack.optimize.UglifyJsPlugin({
    mangle: false,
    sourcemap: false,
  })

  const productionEnv = new webpack.DefinePlugin({
    environment: '"production"',
    'process.env.NODE_ENV': '"production"',
  })

  const copyPlugin = new CopyPlugin([
    {
      from: publicDirectory,
    },
  ])

  const namedModules = new webpack.NamedModulesPlugin()

  if (inDevelopment) {
    return [decko, namedModules]
  } else {
    return [extractSass, decko, uglify, productionEnv, copyPlugin]
  }
}

/** Export configuration **/

module.exports = {
  context: sourceDirectory,
  entry: './js/index.js',
  output: {
    path: buildDirectory,
    filename: 'app.js',
  },
  module: {
    loaders: [
      getBabelLoader(),
      getSassLoader(),
      {
        test: /\.s[ca]ss/,
        loader: 'import-glob-loader',
        enforce: 'pre',
      },
    ],
  },
  plugins: getPlugins(),
  devServer: {
    port: 3300,
    disableHostCheck: true,
    historyApiFallback: {
      index: 'index.html',
    },
    contentBase: publicDirectory,
  },
  resolve: {
    alias: {
      modules: path.resolve(sourceDirectory, 'js/modules'),
      common: path.resolve(sourceDirectory, 'js/common'),
      network: path.resolve(sourceDirectory, 'js/network'),
      player: path.resolve(sourceDirectory, 'js/modules/player/player'),
      react: 'preact-compat',
      'react-dom': 'preact-compat',
      'react-addons-css-transition-group': 'preact-css-transition-group',
    },
  },
}
