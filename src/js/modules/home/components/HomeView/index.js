import preact from 'preact'
import { observer, inject } from 'mobx-react';

// Components
import Button from 'common/components/Button'

@inject('ToastStore')
export default class HomeView extends preact.Component {
  render() {
    return (
      <div style={{ padding: '20px' }}>
        <h2>{'Welcome to WaveDistrict!'}</h2>
        <h4>{'This is still in development, so expect things to not work as expected.'}</h4>
        <br />
        <p>
          {'Join our Discord server: '}
          <a className="link" target="_blank" href="http://discord.gg/NanfSAZ">
            Click here
          </a>
        </p>
        <p>
          {
            'This is just the homepage, but there\'s tracks if you click on "Explore" in the header up top to the right.'
          }
        </p>
        <Button onClick={() => {this.props.ToastStore.addToast('Test!', 3000)}} label="Test toast notification"/>
      </div>
    )
  }
}
