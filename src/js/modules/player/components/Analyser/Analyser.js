import preact from 'preact';
import {logarithmicArray, resizeArray, smoothArray} from 'modules/player/helpers/processFrequencies';
import Player from 'player';

export default class Analyser extends preact.Component {
  constructor(props) {
    super(props);

    this.state = {
      'containerWidth': 0,
    }

    this.Player = Player;
    this.isCompatible = !!this.Player.source.processor;

    this.getCanvasRef = this.getCanvasRef.bind(this);
    this.getContainerRef = this.getContainerRef.bind(this);
    this.drawFrame = this.drawFrame.bind(this);

    this.setChannels();
  }

  setChannels() {
    const
    channels = this.Player.source.processor.analyser.channels,
    {left, right} = channels;

    this.frequencyArray = {
      'left': new Uint8Array(left.frequencyBinCount),
      'right': new Uint8Array(right.frequencyBinCount),
    };

    this.channels = {
      left, right
    };
  }

  processFrequencies(frequencies) {
    frequencies = logarithmicArray(frequencies);
    frequencies = smoothArray(frequencies, 100);
    frequencies = resizeArray(frequencies, (this.canvas.width / this.barMultiplier) + 5);

    return frequencies;
  }

  // Animation
  drawBars(leftFrequencies, rightFrequencies) {
    let
    barPosX = 0,
    barHeight = 0;

    // Process before displaying
    leftFrequencies = this.processFrequencies(leftFrequencies);
    rightFrequencies = this.processFrequencies(rightFrequencies);

    leftFrequencies.map((frequency, index) => {
      const
      left = frequency,
      right = rightFrequencies[index];

      const
      min = Math.min(left, right),
      max = Math.max(left, right);

      const
      difference = min * 0.8,
      differenceMin = min - difference,
      differenceMax = max - difference,
      scaleFactor = max / differenceMax;

      const
      finalMin = differenceMin * scaleFactor,
      finalMax = differenceMax * scaleFactor;

      // These are flipped because you're subtracting:
      const
      heightMin = (finalMin / 2) + this.minimumBarHeight,
      heightMax = (finalMax / 2) + this.minimumBarHeight;

      const
      finalHeightMin = heightMin || this.minimumBarHeight,
      finalHeightMax = heightMax || max;

      barPosX = index * this.barSpacing

      this.canvasContext.fillStyle = 'rgba(255, 255, 255, 0.5)'
      this.canvasContext.fillRect(barPosX, this.canvas.height - finalHeightMax, this.barWidth, finalHeightMax)

      this.canvasContext.fillStyle = 'white'
      this.canvasContext.fillRect(barPosX, this.canvas.height - finalHeightMin, this.barWidth, finalHeightMin)
    })
  }

  drawFrame() {
    if (this.canAnimate) {
      const
      {
        canvasContext,
        channels,
        frequencyArray,
        canvas
      } = this;

      this.barCount = frequencyArray.left.length;
      this.barMultiplier = 8;
      this.barSpacing = 8;
      this.minimumBarHeight = 3;
      this.barWidth = 2;
      this.differenceAmplifier = 10;

      channels.left.getByteFrequencyData(frequencyArray.left);
      channels.right.getByteFrequencyData(frequencyArray.right);

      // Set style
      canvasContext.clearRect(0, 0, canvas.width, canvas.height);
      this.drawBars(frequencyArray.left, frequencyArray.right);
      requestAnimationFrame(this.drawFrame);
    }
  }

  // Refs
  getContainerRef(container) {
    this.container = container;
  }

  getCanvasRef(canvas) {
    this.canvas = canvas;
  }

  setContainerWidth() {
    this.setState({
      'containerWidth': this.container.offsetWidth
    });
  }

  componentDidMount() {
    this.canvasContext = this.canvas.getContext('2d');
    this.canAnimate = true;

    this.setContainerWidth();
    this.drawFrame();
  }

  componentWillUnmount() {
    this.canAnimate = false;
    this.canvasContext = null;
  }

  componentWillUpdate() {
    this.setContainerWidth();
  }

  render() {
    return (
      <div style={{'position': 'relative'}} ref={this.getContainerRef} className="audioAnalyser">
        <canvas style={{'position': 'absolute', 'bottom': '0px'}} width={this.state.containerWidth} ref={this.getCanvasRef}>
          Your browser does not support the analyser or canvas element
        </canvas>
      </div>
    )
  }
}
