import {observable, computed, action} from 'mobx';

class ModalStore {
  @observable items = [];

  @action
  addModal(renderModalContent, options) {
    const key = Math.round(window.performance.now());

    options = Object.assign({
      'icon': null,
      'title': 'ModalTitle',
    }, options);

    this.items.push({
      'key': key,
      'renderModalContent': renderModalContent,
      ...options,
    });

    return key;
  }

  @action
  dismissModal(key) {
    this.items = this.items.filter((modal) => {
      return modal.key !== key;
    });
  }

  @computed
  get hasModals () {
    return this.items.length > 0;
  }
}

const store = new ModalStore;
export default store
