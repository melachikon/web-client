import preact from 'preact'
import { observer, inject } from 'mobx-react'

// Components
import Header from 'modules/core/components/Header'
import Body from 'modules/core/components/Body'
import Footer from 'modules/core/components/Footer'

// Player components
import Transport from 'modules/player/components/Transport'

// Overlay components
import PopoverOverlay from 'modules/popover/components/PopoverOverlay'
import ModalOverlay from 'modules/modal/components/ModalOverlay'
import ToastOverlay from 'modules/toast/components/ToastOverlay'

// Router
import { withRouter } from 'react-router-dom'

const scrollingElement = document.scrollingElement || document.body

@withRouter
@inject('ApplicationStore')
@observer
export default class ApplicationView extends preact.Component {
  constructor() {
    super()
    this.handleMouseMove = this.handleMouseMove.bind(this)
  }

  handleMouseMove(event) {
    window.mouseX = event.clientX
    window.mouseY = event.clientY
  }

  /** Proxy context **/

  getChildContext() {
    return {
      scrollContainer: this,
    }
  }

  /** Proxy methods **/

  scrollTo(pos) {
    scrollingElement.scrollTop = pos
  }

  scrollToBottom() {
    scrollingElement.scrollTop = scrollingElement.scrollHeight
  }

  scrollToTop() {
    scrollingElement.scrollTop = 0
  }

  /** Life cycle **/

  componentDidMount() {
    const { router } = this.context

    this.historyUnlisten = router.history.listen(() => this.scrollToTop())
    window.addEventListener('mousemove', this.handleMouseMove)
  }

  componentWillUnmount() {
    this.historyUnlisten()
  }

  /** Component **/

  render() {
    const { ApplicationStore, children, location } = this.props

    return (
      <div id="wd-app" className="wavedistrict-app">
        <div className="wrapper">
          <Header {...this.props} />
          <div className="body__wrapper">
            <Body {...this.props} routerChildren={children} />
            <Footer />
          </div>
          <Transport />
        </div>
        <div className="overlays">
          <PopoverOverlay />
          <ModalOverlay />
          <ToastOverlay />
        </div>
      </div>
    )
  }
}
