import preact from 'preact';

// Components
import UserList from 'modules/user/components/UserList';

export default class ExploreUserList extends preact.Component {
  render() {
    const { resourceList } = this.props;

    return (
      <UserList resourceList={resourceList}/>
    )
  }
}
