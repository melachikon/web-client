import {observable} from 'mobx'
import FileField from './FileField';

export default class ImageField extends FileField {
  @observable renderedImageString = null;
  @observable hasRendered = false;

  read(file) {
    const reader = new FileReader();

    this.hasRendered = false;
    
    reader.onload = (event) => {
      this.renderedImageString = event.target.result;
      this.hasRendered = true;
    }

    reader.readAsDataURL(file);
  }
}
