import preact from 'preact';
import getClassName from 'classnames';

export default class SliderInput extends preact.Component {
  constructor(props) {
    super(props);

    this.state = {
      'isGrabbing': false
    };

    this.currentValue = props.value || 0;

    this.handleGrab = this.handleGrab.bind(this);
    this.handleRelease = this.handleRelease.bind(this);
    this.handleMove = this.handleMove.bind(this);

    this.getAreaRef = this.getAreaRef.bind(this);
    this.getFillRef = this.getFillRef.bind(this);
    this.getThumbRef = this.getThumbRef.bind(this);
  }

  calculateEventPosition(event) {
    const
    {areaElement} = this,
    width = areaElement.offsetWidth,
    boundingRect = areaElement.getBoundingClientRect(),
    elementLeftOffset = boundingRect.left + document.body.scrollLeft;

    let
    calculatedPosition = event.pageX - elementLeftOffset;
    calculatedPosition = calculatedPosition / width;

    return calculatedPosition;
  }

  // Set value
  setValue(value) {
    const
    {fillElement, thumbElement} = this;

    // Prevent value from going out of bounds
    value = Math.min(Math.max(value, 0), 1);

    fillElement.style.transform = (`scaleX(${value})`);
    thumbElement.style.transform = (`translateX(-${100 - (100 * value)}%)`);

    this.currentValue = value;
  }

  // Mouse event handling
  addMoveListeners() {
    window.addEventListener('mousemove', this.handleMove, true);
    window.addEventListener('touchmove', this.handleMove, true);
  }

  removeMoveListeners() {
    window.removeEventListener('mousemove', this.handleMove, true);
    window.removeEventListener('touchmove', this.handleMove, true);
  }

  calculateByEventType(event) {
    const
    isTouch = /^touch/.test(event.type);

    if (isTouch) this.setValue(this.calculateEventPosition(event.touches[0]));
    else this.setValue(this.calculateEventPosition(event));
  }

  // Event handlers
  handleGrab(event) {
    event.preventDefault();

    this.setState({'isGrabbing': true});
    this.calculateByEventType(event);

    window.addEventListener('mouseup', this.handleRelease, true);
    window.addEventListener('touchend', this.handleRelease, true);
    this.addMoveListeners();

    if (this.props.onGrab) this.props.onGrab(this.currentValue);
    if (this.props.onMove) this.props.onMove(this.currentValue);
  }

  handleRelease(event) {
    event.preventDefault();

    this.setState({'isGrabbing': false});

    window.removeEventListener('mouseup', this.handleRelease, true);
    window.removeEventListener('touchend', this.handleRelease, true);
    this.removeMoveListeners();

    if (this.props.onRelease) this.props.onRelease(this.currentValue);
  }

  handleMove(event) {
    event.preventDefault();

    this.calculateByEventType(event);

    if (this.props.onMove) this.props.onMove(this.currentValue);
  }

  // Refs
  getAreaRef(ref) {
    this.areaElement = ref;
  }

  getFillRef(ref) {
    this.fillElement = ref;
  }

  getThumbRef(ref) {
    this.thumbElement = ref;
  }

  componentDidMount() {
    this.setValue(this.currentValue);
  }

  componentWillReceiveProps(props) {
    if (props.value !== undefined) this.setValue(props.value);
  }

  render() {
    const { variation, readOnly } = this.props;

    const className = getClassName('sliderInput', {
      [`-${variation}`]: !!variation,
      '-readOnly': readOnly,
    });

    return (
      <div className={className}>
        <div ref={this.getAreaRef} onMouseDown={this.handleGrab} onTouchStart={this.handleGrab} className="sliderInput__area">
          <div className="sliderInput__rail">
            <div style={{}} ref={this.getFillRef} className="sliderInput__fill"></div>
            <div style={{}} ref={this.getThumbRef} className="sliderInput__thumbContainer">
              <div className="sliderInput__thumb"></div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
