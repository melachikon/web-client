import preact from 'preact';
import classNames from 'classnames';

import {observable, action} from 'mobx';
import {observer, inject} from 'mobx-react';

import {imageProcessor} from 'common/helpers/image';

@observer
export default class ImageRenderer extends preact.Component {
  @observable hasLoaded = false;
  @observable hasRendered = false;
  @observable imageURL = null;

  constructor() {
    super();

    this.canvas = document.createElement('canvas');
    this.canvasContext = this.canvas.getContext('2d');

    // Create image in memory
    this.image = new Image();
    this.image.onload = this.renderImage;
    this.image.crossOrigin = "anonymous";
  }

  processImage() {
    let
    {blurScale} = this.props,
    imageData = null;

    blurScale = blurScale || 0;

    this.canvas.width = this.image.width;
    this.canvas.height = this.image.height;

    this.canvasContext.drawImage(this.image, 0, 0, this.image.width, this.image.height);

    if (blurScale > 0) imageProcessor.renderBlurredImage(this.canvas, this.canvasContext, blurScale);
  }

  @bind
  renderImage() {
    this.hasRendered = false;

    window.requestAnimationFrame(() => {
      this.processImage();
      this.getImageUrl(this.canvas, (url) => {
        this.imageURL = url;

        this.hasRendered = true;
        this.hasLoaded = true;
      });
    });
  }

  // Resolves either a blob or a base64 string based on what is supported
  getImageUrl(canvas, callback) {
    if (canvas.toBlob) {
      canvas.toBlob((blob) => {
        callback(URL.createObjectURL(blob));
      })

      return;
    }

    if (canvas.toDataURL) {
      return callback(canvas.toDataURL());
    }

    return callback(null);
  }

  rerenderImage(props) {
    this.hasLoaded = false;
    this.hasRendered = false;

    if (this.imageURL) URL.revokeObjectURL(this.imageURL);
    this.image.src = props.src;
  }

  componentWillReceiveProps(props) {
    if (this.props.src !== props.src) {
      this.rerenderImage(props);
    }
  }

  componentWillUnmount() {
    if (this.imageURL) URL.revokeObjectURL(this.imageURL);
  }

  componentDidMount() {
    this.image.src = this.props.src;
  }

  getStyle() {
    let style = {
      'background-size': this.props.scaling || 'contain',
      'width': this.props.width,
      'height': this.props.height,
    };

    if (this.imageURL) {
      style['background-image'] = `url(${this.imageURL})`;
    }

    return style;
  }

  render() {
    const canvasClassName = classNames({
      'imageRenderer': true,
      'hasLoaded': this.hasLoaded,
      'hasRendered': this.hasRendered,
      'hasBlur': !!this.props.blurScale,
    }, this.props.className);

    return (
      <div className={canvasClassName} style={this.getStyle()}></div>
    )
  }
}
