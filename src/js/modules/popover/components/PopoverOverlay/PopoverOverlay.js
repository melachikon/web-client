import preact from 'preact';
import {observer, inject} from 'mobx-react';

import PopoverItem from 'modules/popover/components/PopoverItem';
import Transition from 'react-addons-css-transition-group';

@inject('PopoverStore') @observer
export default class PopoverOverlay extends preact.Component {
  renderPopovers() {
    return this.props.PopoverStore.items.map((popover) => {
      return <PopoverItem popoverKey={popover.key} {...popover}/>
    })
  }

  render() {
    return (
      <div className="popoverOverlay">
        <Transition transitionName="anim__popover" transitionLeaveTimeout={200}>
          {this.renderPopovers()}
        </Transition>
      </div>
    )
  }
}
