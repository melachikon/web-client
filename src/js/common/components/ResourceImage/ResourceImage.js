import preact from 'preact';
import {observer, inject} from 'mobx-react';

import Image from 'common/components/Image';
import CheapImage from 'common/components/CheapImage';

import {MEDIA_URL} from 'network/urls';

// Helpers
import getImageSource from 'common/helpers/image/getImageSource';

@observer
export default class ResourceImage extends preact.Component {
  constructor(props) {
    super(props);

    this.state = {
      'hasSize': false
    };

    this.getRef = this.getRef.bind(this);
  }

  componentDidMount() {
    this.setState({'hasSize': true})
  }

  getRef(ref) {
    this.sizeRef = ref;
  }


  renderExpensiveImage() {
    const {scalingMethod, resource, name, size} = this.props;

    const token = resource.data[name];

    const main = getImageSource(token);
    const preload = getImageSource(token) + '?preload=true';

    return (
      <Image {...this.props} main={main} preload={preload}/>
    )
  }

  renderCheapImage() {
    const {resource, name} = this.props;

    const token = resource.data[name];
    const main = getImageSource(token);

    return (
      <CheapImage {...this.props} src={main}/>
    )
  }

  render() {
    let {size, cheap} = this.props;

    // Empty placeholder to get container width
    if (!this.state.hasSize && size == 'auto') {
      return (
        <div ref={this.getRef} style={{
          'width': '100%',
          'height': '100%',
        }}/>
      )
    }

    if (cheap) return this.renderCheapImage();
    return this.renderExpensiveImage();
  }
}
