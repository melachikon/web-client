import {ResourceList} from 'common/classes/List';

// Stores
import {TrackStore} from 'modules/track/stores';
import {UserStore} from 'modules/user/stores';

class ExploreStore {
  constructor() {
    this.tracks = new ResourceList('/tracks/', TrackStore);
    this.users = new ResourceList('/users/', UserStore);
  }
}

const store = new ExploreStore;
export default store;
