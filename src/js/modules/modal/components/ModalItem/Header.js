import preact from 'preact';

export default class ModalHeader extends preact.Component {
  render() {
    const { title, subtitle } = this.props;

    return (
      <div className="ModalHeader">
        <h1 className="title">{ title }</h1>
        <h3 className="subtitle">{ subtitle }</h3>
      </div>
    );
  }
}
