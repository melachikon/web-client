import preact from 'preact'
import { observer, inject } from 'mobx-react'

import ToastItem from '../ToastItem'
import Transition from 'react-addons-css-transition-group';

@inject('ToastStore')
@observer
export default class ToastOverlay extends preact.Component {
  render() {
    return (
      <div class="ToastOverlay">
        <Transition transitionName="animation">
          { this.renderToasts() }
        </Transition>
      </div>
    )
  }

  renderToasts() {
    const { ToastStore } = this.props
    return ToastStore.toasts.map(toast => {
      const onTimeout = () => {
        ToastStore.removeToast(toast.id)
      }

      const onClick = () => {
        toast.clickAction()
        ToastStore.removeToast(toast.id)
      }

      return (
        <ToastItem
          key={toast.id}
          duration={toast.duration}
          onTimeout={onTimeout}
          onClick={onTimeout}
          children={toast.content}
        />
      )
    })
  }
}
