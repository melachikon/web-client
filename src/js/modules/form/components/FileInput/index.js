import preact from 'preact';
import {observer, inject} from 'mobx-react';
import getClassName from 'classnames';

@observer
export default class FileInput extends preact.Component {
  constructor(props) {
    super(props);

    this.controller = props.controller;
    this.accept = this.controller.accept || props.accept || [];
  }

  @bind
  handleChange(event) {
    const file = event.target.files[0];
    if (!file) return;

    this.controller.setValue(file);
  }

  getLabel() {
    const hasFile = !!this.controller.value;

    if (hasFile) return this.controller.value.name;

    return this.props.label || 'Upload file';
  }

  render() {
    const { controller } = this.props;

    const className = getClassName('FileInput', {
    });

    const inputProps = {
      'type': 'file',
      'onChange': this.handleChange,
      'accept': this.accept.join(', ')
    };

    const label = this.getLabel();

    return (
      <label className={className}>
        <span className="label">{label}</span>
        <input {...inputProps} className="input"/>
      </label>
    );
  }
}
