import preact from 'preact'

// Testing
import { FileServerConnection } from 'network/websocket'
window.testSocket = FileServerConnection

// Import CSS
import '../css/sass/default.scss'

// Import (initialize) player
import Player from 'player'

// Import providers
import StoreProvider from 'modules/core/components/Provider'
import RouteProvider from 'modules/core/components/Router'

// Import databases
import ResourceStoreDB from 'common/databases/ResourceStoreDB'

// Import config and authentication
import { fetchConfig } from 'modules/core/actions'
import { authenticate } from 'modules/auth/actions'

console.log('Initializing...')

/** Debugging */

window.console.warn('Migration from React to Preact is in progress. Expect shit breaking.')
window.Player = Player

/** End debugging **/

class Root extends preact.Component {
  render() {
    return (
      <StoreProvider>
        <RouteProvider />
      </StoreProvider>
    )
  }
}

function initializeViewLayer() {
  preact.render(<Root />, document.body)
}

/** Start initializing **/

fetchConfig()
  .then(p => authenticate())
  .then(p => ResourceStoreDB.open())
  .then(p => initializeViewLayer())
  .then(p => {
    if (window.destroySplash) destroySplash()
  })
  .catch(handleError)

const handleError = error => {
  console.error('An error occured while initializing the application!')
  console.error(error)

  window.alert('An error occured. Try again?')
}

// temporary, for testing splash screen error handling
// dicks()
