import preact from 'preact';
import {observer, inject} from 'mobx-react';

// Components
import SliderInput from 'common/components/SliderInput';

@observer
export default class TrackUploadItemStatus extends preact.Component {
  getStatusText() {
    const { item } = this.props;
    const { audio } = item.form.fields;
    const { uploader } = audio;

    const hasSelectedFile = !!audio.value;

    if (!hasSelectedFile) return 'Select an audio file to begin.';

    if (uploader.status == 'uploading') return 'Uploading...';
    if (uploader.status == 'queued') return 'Waiting...';
    if (uploader.status == 'processing') return 'Processing...';
    if (uploader.status == 'error') return 'Something went wrong. Try again?';
    if (uploader.status == 'completed') return 'Ready to submit.';
  }

  getStatusProgress() {
    const { audio } = this.props.item.form.fields;

    console.log(audio.uploader.progress);

    return audio.uploader.progress;
  }

  render() {
    return (
      <div className="TrackUploadStatus">
        <div className="statusProgress">
          <SliderInput readOnly={true} variation="accented" value={this.getStatusProgress()}/>
        </div>
        <div className="statusText">{this.getStatusText()}</div>
      </div>
    );
  }
}
