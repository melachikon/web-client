import preact from 'preact'
import { Provider } from 'mobx-react'

import { ApplicationStore } from 'modules/core/stores'
import { AuthStore } from 'modules/auth/stores'
import { ExploreStore } from 'modules/explore/stores'
import { ModalStore } from 'modules/modal/stores'
import { PopoverStore } from 'modules/popover/stores'
import { PlayerStore } from 'modules/player/stores'
import { ToastStore } from 'modules/toast/stores'
import { TrackStore, TrackUploadStore } from 'modules/track/stores'
import { UserStore } from 'modules/user/stores'

const Stores = {
  ApplicationStore,
  AuthStore,
  ExploreStore,
  ModalStore,
  PopoverStore,
  PlayerStore,
  ToastStore,
  TrackStore,
  TrackUploadStore,
  UserStore,
}

window.stores = Stores

export default class StoreProvider extends preact.Component {
  render() {
    return <Provider {...Stores}>{this.props.children}</Provider>
  }
}
