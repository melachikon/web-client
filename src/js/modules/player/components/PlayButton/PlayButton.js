import preact from 'preact';
import {observer, inject} from 'mobx-react';
import classNames from 'classnames';

import {injectPlayer} from 'modules/player/hoc';

import Icon from 'common/components/Icon';

@injectPlayer @observer
export default class PlayButton extends preact.Component {
  constructor(props) {
    super(props);

    this.Player = props.player;
    this.handleClick = this.handleClick.bind(this);
  }

  setQueue() {
    const
    {playerQueue} = this.context,
    {name, endpoint, data} = playerQueue,
    {index, player, resource} = this.props,
    {queue} = player;

    const finalData = {
      ...data,
      'offset': index,
      'track': resource
    };

    queue.set(name, endpoint, finalData);
  }

  handleClick() {
    const
    {resource} = this.props;

    if (resource.isActive) {
      this.Player.toggle();
    }

    if (!resource.isActive) {
      this.Player.setTrack(resource)
      this.Player.play();
    }

    if (this.context.playerQueue) this.setQueue();
  }

  renderPlayButtonContent(resource) {
    if (resource.isPlaying && this.Player.isBuffering) {
      return (
        <div className="playButton__spinner"></div>
      );
    }

    if (!resource.isPlaying) return (
      <Icon name="player.play"/>
    );

    if (resource.isPlaying) return (
      <Icon name="player.pause"/>
    );
  }

  render() {
    const
    {resource, className} = this.props,
    rootDivClassName = classNames((className || 'playButton'), {
      'isActive': resource.isActive,
      'isPlaying': resource.isPlaying
    });

    return (
      <a role="button" className={rootDivClassName} onClick={this.handleClick}>
        {this.renderPlayButtonContent(resource)}
      </a>
    )
  }
}
