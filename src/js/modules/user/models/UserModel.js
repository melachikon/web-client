import {ResourceModel} from 'common/classes/Model';
import {ResourceList} from 'common/classes/List';

// Stores
import {TrackStore} from 'modules/track/stores';

import {parseMediaObject} from 'common/helpers/media';

export default class User extends ResourceModel {
  onInitialize(data) {
    this.permalink = `/users/${data.id}`;
    this.clientLink = `/@${data.username}`;

    this.tracks = new ResourceList(this.permalink + '/tracks/', TrackStore);
  }

  onUpdate(data) {
    this.media = parseMediaObject(this.data.media, ['avatar']);
  }
}
