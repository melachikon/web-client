import {observable, action} from 'mobx';

class PopoverStore {
  @observable items = [];

  @action addPopover(renderPopoverContent, options, onOpen, onClose) {
    const key = Math.round(window.performance.now());

    this.items.push({
      'key': key,
      'renderPopoverContent': renderPopoverContent,
      'options': {
        'type': 'cursor',
        ...options
      },
      'onOpen': onOpen,
      'onClose': onClose
    });

    return key;
  }

  @action dismissPopover(key) {
    this.items = this.items.filter((popover) => {
      if (popover.key == key) {
        popover.onClose();
        return false;
      }

      return true;
    });
  }
}

const store = new PopoverStore;
export default store
