import { observable, action, observe } from 'mobx'

import AudioSource from 'modules/player/classes/AudioSource'
import Queue from 'modules/player/classes/Queue'

// Helpers
import getAudioSource from 'modules/player/helpers/getAudioSource'

// Stores
import { TrackStore } from 'modules/track/stores'

class PlayerStore {
  source = new AudioSource()
  queue = new Queue()

  @observable isPlaying = false
  @observable volume = 1
  @observable currentTrack = null

  @observable isBuffering = false
  lastBufferCheck = null

  @observable hasFailed = false

  @observable isShuffling = false
  @observable isRepeating = false

  constructor() {
    // Initialization
    this.setVolume(localStorage.playerVolume || this.volume)
    this.runBufferCheck()
    this.runPlayerTimer()
    this.bindListeners()

    this.resumePlayback()
  }

  // Setter methods
  @bind
  @action
  setTrack(track) {
    const source = getAudioSource(track.data.audio, 320)

    this.currentTrack = track
    this.source.setAudioURL(source)
    localStorage.playerCurrentTrack = track.data.id
  }

  @bind
  @action
  setVolume(volume) {
    this.volume = localStorage.playerVolume = volume
    this.source.setVolume(volume)
  }

  @bind
  @action
  setTime(time) {
    this.source.setTime(time)
    this.lastBufferCheck = time
  }

  // Event listeners
  bindListeners() {
    this.source.audio.addEventListener('ended', this.handleEnded)
    this.source.audio.addEventListener('error', this.handlePlaybackError)
  }

  // Event handlers
  @bind
  handleEnded() {
    const nextTrack = this.queue.goNext()

    if (nextTrack) {
      this.setTrack(nextTrack)
      this.play()
      return
    }

    return this.pause()
  }

  @bind
  handlePlaybackError(error) {
    console.log(error)

    this.hasFailed = true

    return this.pause()
  }

  // Transport
  @bind
  @action
  play() {
    this.isPlaying = true

    this.source
      .play()
      .then()
      .catch(this.handlePlaybackError)

    sessionStorage.playerPlayState = 'playing'
  }

  @bind
  @action
  pause() {
    this.isPlaying = false
    this.source.pause()
    sessionStorage.playerPlayState = 'paused'
  }

  @bind
  @action
  toggle() {
    const action = this.isPlaying ? 'pause' : 'play'
    this[action]()
  }

  @bind
  @action
  next() {
    const nextTrack = this.queue.goNext()

    if (nextTrack) {
      this.setTrack(nextTrack)
      this.play()
    }
  }

  @bind
  @action
  previous() {
    const previousTrack = this.queue.goPrevious()

    if (previousTrack) {
      this.setTrack(previousTrack)
      this.play()
    }
  }

  @bind
  @action
  toggleShuffle() {
    this.isShuffling = !this.isShuffling
  }

  @bind
  @action
  toggleRepeat() {
    this.isRepeating = !this.isRepeating
  }

  @bind
  @action
  goto(queueIndex, trackIndex) {
    const track = this.queue.setIndex(queueIndex, trackIndex)

    if (track) {
      this.setTrack(track)
      this.play()
    }
  }

  // Buffer checks
  @bind
  runBufferCheck() {
    this.checkIfBuffering()
    setTimeout(this.runBufferCheck, 300)
  }

  checkIfBuffering() {
    if (this.isPlaying) {
      this.isBuffering = this.lastBufferCheck == this.currentTime
      return (this.lastBufferCheck = this.currentTime)
    }

    return (this.isBuffering = false)
  }

  // Timer
  @bind
  runPlayerTimer() {
    if (this.isPlaying) {
      localStorage.playerCurrentTime = this.currentTime
    }

    requestAnimationFrame(this.runPlayerTimer)
  }

  // Resume
  @bind
  resumePlayback() {
    const trackId = localStorage.playerCurrentTrack,
      trackTime = localStorage.playerCurrentTime,
      playState = sessionStorage.playerPlayState

    if (trackId) {
      const track = TrackStore.fetchItem(trackId)

      const disposable = observe(track, 'isFetched', change => {
        if (change.newValue) {
          this.setTrack(track)
          this.setTime(trackTime)

          if (playState == 'playing') this.play()
        }

        disposable()
      })
    }
  }

  // Getters
  get currentTime() {
    return this.source.currentTime
  }
}

export default new PlayerStore()
