import preact from 'preact';

export default class ModalFooter extends preact.Component {
  render() {
    return (
      <div className="ModalFooter">
        {this.props.children}
      </div>
    );
  }
}
