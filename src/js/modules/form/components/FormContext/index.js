import preact from 'preact';

export default class FormContext extends preact.Component {
  getChildContext() {
    return {
      'form': this.props.form,
    };
  }

  render() {
    return (
      <div data-form className="FormContext">
        {this.props.children}
      </div>
    );
  }
}
