import {ApiServer, MediaServer} from 'network/http';
import {MediaServerConnection} from 'network/websocket';

import {AuthStore} from 'modules/auth/stores';
import {UserStore} from 'modules/user/stores';

export default function authenticate() {
  return new Promise((resolve, reject) => {
    const token = localStorage.authToken;

    if (!token) {
      return resolve();
    }

    const headers = {
      'Authorization': 'Bearer ' + token
    };

    ApiServer.setHeaders(headers);
    MediaServer.setHeaders(headers);

    ApiServer.GET('/auth/user/')
    .then((response) => {
      if (!response.data.anonymous) {
        AuthStore.currentUser = UserStore.addItem(response.data.user);

        // Authenticate websockets
        MediaServerConnection.login(token);
      } else {
        localStorage.removeItem('authToken');
      }

      resolve();
    })
  })
}
