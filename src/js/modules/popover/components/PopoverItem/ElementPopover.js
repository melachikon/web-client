import preact from 'preact';
import {alignIfRequired} from 'modules/popover/helpers';

export default class ElementPopover extends preact.Component {
  constructor(props) {
    super(props);

    this.options = props.options;

    this.getPopoverRef = this.getPopoverRef.bind(this);
    this.getPopoverItemRef = this.getPopoverItemRef.bind(this);
    this.handleWindowResize = this.handleWindowResize.bind(this);
  }

  componentDidMount() {
    this.props.onOpen(this.popoverRef, this.props.popoverKey);
    this.setPosition();

    window.addEventListener('resize', this.handleWindowResize, true);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize, true);
  }

  handleWindowResize() {
    this.setPosition();
  }

  getPopoverRef(ref) {
    this.popoverRef = ref;
  }

  getPopoverItemRef(ref) {
    this.popoverItemRef = ref;
  }

  setPosition() {
    const
    elementRef = this.props.options.element,
    elementRect = elementRef.getBoundingClientRect(),
    containerRef = this.popoverRef;

    containerRef.style.width = elementRect.width + 'px';
    containerRef.style.height = elementRect.height + 'px';

    if (this.options.fixed) {
      containerRef.style.top = elementRect.top + 'px';
      containerRef.style.left = elementRect.left + 'px';
    }

    // If the element is not fixed, offset by scroll
    if (!this.options.fixed) {
      containerRef.style.top = (elementRect.top + window.scrollY) + 'px';
      containerRef.style.left = (elementRect.left + window.scrollX) + 'px';
    }

    alignIfRequired(this.popoverRef, this.popoverItemRef, this.props.options.preferredAlignment);
  }

  render() {
    const
    positioning = this.options.fixed ? 'fixed' : 'absolute',
    popoverStyle = {
      'position': positioning,
      'zIndex': '500',
      'pointerEvents': 'none',
    };

    return (
      <div id={`popover-${this.props.popoverKey}`} style={popoverStyle} ref={this.getPopoverRef}>
        <div ref={this.getPopoverItemRef} className="popoverItem elementPopover">
          <div className="popoverItem__content">
            {this.props.renderPopoverContent()}
          </div>
        </div>
      </div>
    )
  }
}
