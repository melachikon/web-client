import preact from 'preact';
import {observer, inject} from 'mobx-react';

// Helpers
import {convertSecondsToHHMMSS} from 'modules/player/helpers/convertTime';

// Component
import SliderInput from 'common/components/SliderInput';

@observer
export default class UploadTimeModule extends preact.Component {
  render() {
    const { uploadTime, usedUploadTime } = this.props.user.data.quota;

    let displayText, remainingProgress;

    const remainingTime = (uploadTime - usedUploadTime);

    const displayRemainingTime = convertSecondsToHHMMSS(remainingTime);
    const displayTotalTime = convertSecondsToHHMMSS(uploadTime);

    const hasTimeLeft = (remainingTime > 0);

    if (hasTimeLeft) {
      displayText = `You have ${displayRemainingTime} out of ${displayTotalTime} left of upload time.`;
      remainingProgress = (usedUploadTime / uploadTime);
    } else {
      displayText = 'You are out of upload time. Sorry.';
      remainingProgress = 1;
    }

    return (
      <div className="UploadTimeModule">
        <div className="title">Upload time</div>
        <div className="body">
          <p className="Paragraph">
            {displayText}
          </p>
          <div className="progress">
            <SliderInput readOnly={true} variation="accented" value={remainingProgress}/>
          </div>
        </div>
      </div>
    );
  }
}
