import {AuthStore} from 'modules/auth/stores';

function getLinkData(path, icon, text, condition, isExact) {
  return {
    'path': path,
    'icon': icon,
    'text': text,
    'condition': condition,
    'isExact': isExact,
  };
}

export function getHeaderLinks() {
  let
  links = [],
  currentUser = AuthStore.currentUser;

  links.push(
    getLinkData('/', 'app.home', 'Home', true, true),
    getLinkData('/explore', 'app.explore', 'Explore', true, false),
  );

  return links;
}
