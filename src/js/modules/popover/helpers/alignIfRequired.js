import {getElementViewportOffset} from 'common/helpers/element';

// This assumes the element is inside a relatively positioned parent
export default function alignIfRequired(anchor, child, preferred = {}) {
  const
  offset = getElementViewportOffset(anchor, 10),
  style = {};

  const
  isOverflowingRight = (offset.left + child.offsetWidth) > (window.innerWidth - 30),
  isOverflowingBottom = (offset.top + child.offsetHeight) > (window.innerHeight - 30);

  let
  horizontal = preferred.horizontal || 'left',
  vertical = preferred.vertical || 'top';

  horizontal = isOverflowingRight ? 'right' : horizontal,
  vertical = isOverflowingBottom ? 'bottom' : vertical;


  // Reset
  child.style.top = null;
  child.style.bottom = null;
  child.style.left = null;
  child.style.right = null;

  child.style[horizontal] = '0px';
  child.style[vertical] = `${anchor.offsetHeight}px`;
}
