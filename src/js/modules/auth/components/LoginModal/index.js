import preact from 'preact';
import getClassName from 'classnames';

// Modal components
import ModalHeader from 'modules/modal/components/ModalItem/Header';
import ModalFooter from 'modules/modal/components/ModalItem/Footer';

// Components
import Button from 'common/components/Button';

export default class LoginModal extends preact.Component {
  render() {
    return (
      <div className="LoginModal">
        <ModalHeader title="Login" subtitle="Enter your username and password"/>
        <ModalFooter>
          <Button onClick={this.props.dismiss} label="Cancel"/>
        </ModalFooter>
      </div>
    )
  }
}
