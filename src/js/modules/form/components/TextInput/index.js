import preact from 'preact';
import {observer, inject} from 'mobx-react';
import getClassName from 'classnames';

@observer
export default class TextInput extends preact.Component {
  constructor(props) {
    super(props);

    this.state = {
      'hasFocus': false,
    };

    this.controller = props.controller;
  }

  @bind
  handleFocus() {
    this.setState({'hasFocus': true});
  }

  @bind
  handleBlur() {
    this.setState({'hasFocus': false});
  }

  @bind
  handleInput(event) {
    this.setValue(event.target.value);
  }

  getValue() {
    if (this.controller) return this.controller.getValue();
    if (this.props.value) return this.props.value;

    return 'null';
  }

  setValue(value) {
    if (this.controller) this.controller.setValue(value);
    if (this.props.onChange) this.props.onChange(value);
  }

  render() {
    const { controller } = this.props;

    const className = getClassName('TextInput', {
      '-hasFocus': this.state.hasFocus,
      '-isMultiline': this.props.multiline,
    });

    const inputProps = {
      'onFocus': this.handleFocus,
      'onBlur': this.handleBlur,
      'onInput': this.handleInput,
      'value': this.getValue(),
      'rows': this.props.rows,
    };

    let type = this.props.type || 'text';
    const Input = this.props.multiline ? 'textarea' : 'input';

    return (
      <div className={className}>
        <Input {...inputProps} className="input" type={type}/>
      </div>
    );
  }
}
