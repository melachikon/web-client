import preact from 'preact';
import {observer, inject} from 'mobx-react';

// Hoc
import {wrapInMediaQuery, injectResource} from 'common/hoc/';

// Components
import Page from 'modules/core/components/Page';

// Related components
import UserViewBanner from './Banner';
import UserViewSidebar from './Sidebar';
import UserViewContent from './Content';

@injectResource('UserStore', (props) => {
  return props.UserStore.fetchItemByReference(props.match.params.username);
})
@observer
export default class UserView extends preact.Component {
  render() {
    const { resource } = this.props;

    return (
      <Page name="UserView">
        <div className="header">
          <UserViewBanner {...this.props}/>
        </div>
        <div className="body">
          <div className="sidebar">
            <UserViewSidebar {...this.props}/>
          </div>
          <div className="content">
            <UserViewContent {...this.props}/>
          </div>
        </div>
      </Page>
    )
  }
}
