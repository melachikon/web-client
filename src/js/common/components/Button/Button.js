import preact from 'preact';
import classnames from 'classnames';

import Icon from 'common/components/Icon';

export default class Button extends preact.Component {

  @bind
  handleClick() {
    const {onClick} = this.props;

    if (onClick) onClick();
  }

  renderContent() {
    const {label, icon} = this.props;
    const renderedContentArray = [];

    if (this.props.children.length) return this.props.children;

    if (icon) renderedContentArray.push(
      <div className="icon">
        <Icon name={icon}/>
      </div>
    );

    if (label) renderedContentArray.push(
      <div className="label">{label}</div>
    );

    return renderedContentArray;
  }

  render() {
    const {isActive, isDisabled, variants} = this.props;

    // Set default type
    let type = this.props.type || 'DefaultButton';

    const buttonClasses = classnames(type, {
      '-isActive': isActive,
      '-isDisabled': isDisabled,
      ...variants,
    });

    const buttonProps = {
      'className': buttonClasses,
      'onClick': this.handleClick,
    };

    const children = this.renderContent();

    return (
      <a role="button" {...buttonProps}>
        {children}
      </a>
    )
  }
}
