import preact from 'preact'
import { observer, inject } from 'mobx-react'
import bem from 'bem-classname'

// Related components
import QueueItem from './QueueItem'

// HOC
// import { injectPlayer } from 'modules/player/hoc'

// Bind classnames
const classNames = bem.bind(null, 'queueList')

@inject('PlayerStore')
@observer
export default class QueueList extends preact.Component {
  renderList() {
    const { queue } = this.props.PlayerStore
    const hasItems = queue.items.length > 0

    if (hasItems) {
      return queue.items.map((queueItem, queueIndex) => {
        const isActive = queueIndex == queue.index

        return (
          <QueueItem
            key={queueItem.id}
            isActive={isActive}
            queueIndex={queueIndex}
            item={queueItem}
          />
        )
      })
    }
  }

  render() {
    return <div className={classNames()}>{this.renderList()}</div>
  }
}
