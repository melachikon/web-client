import * as appIcons from './appIcons';
import * as playerIcons from './playerIcons';

export default {
  'app': appIcons,
  'player': playerIcons,
};
