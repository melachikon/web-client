import preact from 'preact';

// Components
import ResourceImage from 'common/components/ResourceImage';

// Router
import {NavLink} from 'react-router-dom'

export default class UserGridItem extends preact.Component {
  render() {
    const { resource } = this.props;

    const avatarImageProps = {
      'width': '100%',
      'height': '100%',
      'resource': resource,
      'name': 'avatar',
      'size': 'auto',
      'cheap': true,
    };

    return (
      <div className="UserGridItem">
        <NavLink to={resource.clientLink} className="avatar">
          <ResourceImage {...avatarImageProps}/>
        </NavLink>
        <div className="displayName">
          <NavLink to={resource.clientLink}>
            {resource.data.displayName}
          </NavLink>
        </div>
      </div>
    )
  }
}
