import preact from 'preact';
import {observer, inject} from 'mobx-react';

// Components
import Page from 'modules/core/components/Page';
import PageHeader from 'modules/core/components/Page/Header';
import PageBody from 'modules/core/components/Page/Body';
import Button from 'common/components/Button';

import TrackUploadList from './TrackUploadList'
import UploadTimeModule from './UploadTimeModule';

@inject('AuthStore', 'TrackUploadStore') @observer
export default class TrackUploadView extends preact.Component {
  @bind
  handleNewClick() {
    this.props.TrackUploadStore.add();
  }

  render() {
    const user = this.props.AuthStore.currentUser;

    const newButtonVariants = {
      '-block': true,
    }

    return (
      <Page name="TrackUploadView">
        <PageHeader icon="app.upload" title="Upload"></PageHeader>
        <PageBody>
          <div className="list">
            <TrackUploadList/>
            <div className="button">
              <Button variants={newButtonVariants} onClick={this.handleNewClick} label="Create upload"/>
            </div>
          </div>
          <div className="sidebar">
            <UploadTimeModule user={user}/>
          </div>
        </PageBody>
      </Page>
    )
  }
}
