import preact from 'preact';
import {alignIfRequired} from 'modules/popover/helpers';

export default class CursorPopover extends preact.Component {
  constructor() {
    super();

    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.getPopoverRef = this.getPopoverRef.bind(this);
    this.getPopoverItemRef = this.getPopoverItemRef.bind(this);
  }

  componentDidMount() {
    window.addEventListener('mousemove', this.handleMouseMove, true);
    this.props.onOpen(this.popoverRef, this.props.popoverKey);

    this.setPosition(window.mouseX, window.mouseY);
  }

  componentWillUnmount() {
    window.removeEventListener('mousemove', this.handleMouseMove, true);
  }

  getPopoverRef(ref) {
    this.popoverRef = ref;
  }

  getPopoverItemRef(ref) {
    this.popoverItemRef = ref;
  }

  handleMouseMove(event) {
    this.mouseX = event.clientX;
    this.mouseY = event.clientY;

    this.setPosition(this.mouseX, this.mouseY);
  }

  setPosition(x, y) {
    this.popoverRef.style.top = `${y}px`;
    this.popoverRef.style.left = `${x}px`;

    alignIfRequired(this.popoverRef, this.popoverItemRef, this.props.options.preferredAlignment);
  }

  render() {
    const popoverStyle = {
      'position': 'fixed',
      'pointerEvents': 'none',
      'zIndex': '500',
    };

    return (
      <div id={`popover-${this.props.popoverKey}`} style={popoverStyle} ref={this.getPopoverRef}>
        <div ref={this.getPopoverItemRef} className="popoverItem cursorPopover">
          <div className="popoverItem__content">
            {this.props.renderPopoverContent()}
          </div>
        </div>
      </div>
    )
  }
}
