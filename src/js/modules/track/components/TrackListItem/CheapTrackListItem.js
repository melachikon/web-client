import preact from 'preact';
import {observer, inject} from 'mobx-react';
import classNames from 'classnames';

// Hoc
import {wrapInLazyLoadHandler} from 'common/hoc';
import {injectPlayer} from 'modules/player/hoc';

// Components
import ResourceImage from 'common/components/ResourceImage';

const renderPlaceholder = () => {
  return (
    <div className="trackListItem__placeholder"></div>
  )
}

@wrapInLazyLoadHandler(renderPlaceholder)
@injectPlayer @observer
export default class CheapTrackListItem extends preact.Component {
  constructor(props) {
    super(props)

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const
    {player} = this.props;

    player.setTrack(this.props.resource);
    player.play();

    this.setQueue();
  }

  setQueue() {
    const
    {playerQueue} = this.context,
    {name, endpoint, data} = playerQueue,
    {index, player, resource} = this.props,
    {queue} = player;

    const finalData = {
      ...data,
      'offset': index,
    };

    queue.add(name, endpoint, finalData);
  }

  render() {
    const
    {resource} = this.props,
    {data} = resource,
    rootDivClassName = classNames('trackListItem', {
      'isActive': resource.isActive
    });

    const
    artworkProps = {
      'width': '100%',
      'height': '100%',
      'name': 'artwork',
      'size': 'small',
      'resource': resource,
      'cheap': true,
    };


    return (
      <li onClick={this.handleClick} className={rootDivClassName}>
        <div className="trackListItem__artwork">
          <ResourceImage {...artworkProps}/>
        </div>
        <div className="trackListItem__primaryInfo">
          <div className="trackListItem__title">{data.title}</div>
          <div className="trackListItem__user">{resource.user.data.display_name}</div>
        </div>
      </li>
    )
  }
}
