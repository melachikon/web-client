import preact from 'preact';

// HOC
import {wrapInListHandler} from 'common/hoc';

// Components
import UserGridItem from 'modules/user/components/UserGridItem';

@wrapInListHandler
export default class UserList extends preact.Component {
  render() {
    const renderedList = this.props.items.map((resource, index) => {
      return <UserGridItem index={index} key={resource.data.id} resource={resource}/>;
    })

    return (
      <ul className="UserList">
        {renderedList}
      </ul>
    );
  }
}
