import preact from 'preact';
import {observer, inject} from 'mobx-react';

import FixedPopoverTrigger from 'common/components/FixedPopoverTrigger';
import ResourceImage from 'common/components/ResourceImage';

export default class UserButton extends preact.Component {
  renderPopoverContent() {
    return 'UserButtonPopoverContent';
  }

  render() {
    const preferredAlignment = {
      horizontal: 'right'
    };

    return (
      <FixedPopoverTrigger
        isFixed
        preferredAlignment={preferredAlignment}
        renderPopoverContent={this.renderPopoverContent}
      >
        <div className="UserButton">
          <div className="image">
            <ResourceImage
              height="100%"
              width="100%"
              resource={this.props.user}
              name="avatar"
              size="small"
            />
          </div>
        </div>
      </FixedPopoverTrigger>
    )
  }
}
