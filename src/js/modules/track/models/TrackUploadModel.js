import {observable, observe} from 'mobx';
import {ApiServer} from 'network/http';

// Stores
import {TrackUploadStore} from 'modules/track/stores';

// Forms
import TrackUploadForm from 'modules/track/forms/TrackUploadForm';

export default class TrackUploadModel {
  @observable currentTab = 'basic';
  @observable autoSubmit = false;
  @observable isReady = false;

  constructor() {
    this.id = window.performance.now();
    this.form = new TrackUploadForm();

    observe(this.form, 'isReady', this.handleReady);
  }

  save() {
    this.form.validate().then(isValid => {
      if (isValid) {
        this.autoSubmit = true;
        if (this.isReady) this.submit();
      }
    });
  }

  submit() {
    const data = Object.assign({
      'type': 'Other',
    }, this.form.getSerializedData());

    ApiServer.POST('/tracks', data).then(response => {
      this.remove();
    });
  }

  remove() {
    TrackUploadStore.remove(this.id);
  }

  @bind
  handleReady(value) {
    this.isReady = !!value.newValue;

    if (this.autoSubmit && this.isReady) this.submit();
  }
}
