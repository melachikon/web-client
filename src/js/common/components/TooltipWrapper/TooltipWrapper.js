import preact from 'preact';
import {observer, inject} from 'mobx-react';

@inject('PopoverStore') @observer
export default class TooltipWrapper extends preact.Component {
  constructor(props) {
    super(props);

    this.state = {
      'popoverKey': null,
      'isOpen': false,
      'isHovering': false,
    };

    this.delay = props.delay || 500;
    this.PopoverStore = props.PopoverStore;

    this.showTooltip = this.showTooltip.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
    this.handleLeave = this.handleLeave.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
  }

  componentWillUnmount() {
    if (this.state.isOpen)
    this.dismissTooltip();

    window.removeEventListener('mousemove', this.handleMouseMove, true);
  }

  showTooltip() {
    const onOpen = (ref, key) => {
      this.setState({
        'popoverKey': key,
        'popoverRef': ref,
        'isOpen': true
      });
    };

    const onClose = () => {
      this.setState({
        'popoverKey': null,
        'popoverRef': null,
        'isOpen': false,
      });
    };

    const renderPopoverContent = () => {
      return this.props.text || 'No tooltip text';
    }

    const options = {
      'type': 'cursor'
    };

    this.PopoverStore.addPopover(renderPopoverContent, options, onOpen, onClose);
  }

  dismissTooltip() {
    this.PopoverStore.dismissPopover(this.state.popoverKey);

    this.setState({
      'popoverKey': null,
      'isOpen': false,
      'hasDismissed': true,
    });
  }

  clearTimer() {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }

  startTimer() {
    if (!this.state.isOpen && !this.timeout)
    this.timeout = setTimeout(this.showTooltip, this.delay);
  }

  handleEnter() {
    this.startTimer();
    this.setState({'isHovering': true})

    window.addEventListener('mousemove', this.handleMouseMove, true);
  }

  handleMouseMove() {
    if (!this.state.hasDismissed) {
      this.clearTimer();
      this.startTimer();
    }
  }

  handleLeave(event) {
    this.clearTimer();

    if (this.state.isOpen)
    this.dismissTooltip();

    this.setState({'isHovering': false, 'hasDismissed': false})
    window.removeEventListener('mousemove', this.handleMouseMove, true);
  }

  handleClick() {
    this.clearTimer();

    if (this.state.isOpen)
    this.dismissTooltip();

    // Set dismiss state anyway
    if (!this.state.isOpen)
    this.setState({
      'hasDismissed': true
    });
  }

  render() {
    return (
      <div onMouseEnter={this.handleEnter} onClick={this.handleClick} onMouseLeave={this.handleLeave} data-tooltip-trigger>
        {this.props.children}
      </div>
    )
  }
}
