import preact from 'preact';

// Components
import ResourceImage from 'common/components/ResourceImage';


export default class UserViewBanner extends preact.Component {
  render() {
    const { resource } = this.props;

    const imageProps = {
      'resource': resource,
      'name': 'avatar',
      'width': '500px',
      'height': '500px',
      'blur': '100',
      'size': 'preload',
    };

    return (
      <div className="UserViewBanner">
        <div className="image">
          <ResourceImage {...imageProps}/>
        </div>
        <div className="filter"/>
      </div>
    );
  }
}
