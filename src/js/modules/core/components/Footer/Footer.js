import preact from 'preact';

export default class Footer extends preact.Component {

  renderLogo() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1066.09 1092.1">
        <g>
          <path d="M1066.09,208v546c0,186.69-151.34,338-338,338s-338-151.34-338-338V276.93a26,26,0,0,1,26-26h52c101.78,0,186.49,73.1,204.49,169.65a209.22,209.22,0,0,1,3.53,38.36V754.07a52,52,0,1,0,104,0V26a26,26,0,0,1,26-26h52C973,0,1066.09,93.13,1066.09,208Z" fill="#ffffff"/>
          <path d="M84.68.11C82.47,0,80.23,0,78,0H26A26,26,0,0,0,0,26v546c0,114.89,93.13,208,208,208h52a26,26,0,0,0,26-26V208C286,95.37,196.48,3.64,84.68.11Z" fill="#ffffff"/>
        </g>
      </svg>
    );
  }

  renderSocialLinks() {
    const linksToRender = [
      {
        'href': 'https://discord.gg/NanfSAZ',
        'img': '/img/vendor/discord.svg',
      },
      {
        'href': 'https://twitter.com/wavedistrictapp',
        'img': '/img/vendor/twitter.svg',
      },
      {
        'href': 'https://www.patreon.com/enitoni',
        'img': '/img/vendor/patreon.svg',
      },
      {
        'href': 'https://www.reddit.com/r/wavedistrict/',
        'img': '/img/vendor/reddit.svg',
      }
    ];

    return linksToRender.map((link) => {
      return (
        <a href={link.href} target="blank" className="footer__socialLinkItem">
          <img src={link.img} className="footer__socialLinkItemImage"/>
        </a>
      );
    });
  }

  render() {
    return (
      <footer className="Footer">
        <div className="footer__body">
          <div className="footer__content">
            <div className="footer__links"></div>
            <div className="footer__social">
              <div className="footer__socialLinks">
                {this.renderSocialLinks()}
              </div>
            </div>
          </div>
          <div className="footer__watermark">
            {this.renderLogo()}
          </div>
        </div>
        <div className="footer__bottom">
          <div className="footer__version">{"Alpha v2.0"}</div>
          <div className="footer__mark">WaveDistrict, { new Date().getFullYear() }</div>
        </div>
      </footer>
    )
  }
}
