import {ApiServer} from 'network/http';
import {ApplicationStore} from 'modules/core/stores';

// Promise function
export default function fetchConfig() {
  return new Promise((resolve, reject) => {
    // On the splashscreen, show the user that the app is connecting to server.
    if (window.splashStatusText) window.splashStatusText.innerHTML = 'Connecting...';
    resolve();

    // If config is cached in localStorage, return an early resolve
    if (localStorage.applicationConfig) {
      ApplicationStore.config = JSON.parse(localStorage.applicationConfig);
    }

    ApiServer.GET('/config')
    .then((response) => {
      // Set the config in store
      ApplicationStore.config = response.data;
      localStorage.applicationConfig = JSON.stringify(ApplicationStore.config);
      resolve();
    })
  })
}
