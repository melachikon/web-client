import preact from 'preact';
import {observer, inject} from 'mobx-react';

// Components
import TooltipWrapper from 'common/components/TooltipWrapper';
import FixedPopoverTrigger from 'common/components/FixedPopoverTrigger';
import ResourceImage from 'common/components/ResourceImage';
import Icon from 'common/components/Icon';
import Button from 'common/components/Button';

import NavButton from './NavButton';
import UserButton from './UserButton';

// Modals
import RegisterModal from 'modules/auth/components/RegisterModal';
import LoginModal from 'modules/auth/components/LoginModal';

// Routing
import {NavLink} from 'react-router-dom';
import {getHeaderLinks} from 'modules/core/helpers/header';

// HOC
import injectCurrentUser from 'modules/auth/hoc/injectCurrentUser';

@injectCurrentUser
@inject('ModalStore')
@observer
export default class HeaderNav extends preact.Component {
  renderNavLink(path, icon, text, condition, isExact) {
    if (condition) return (
      <TooltipWrapper key={path} text={text}>
        <NavLink exact={isExact} to={path} className="NavLink" activeClassName="-isSelected">
          <div className="icon">
            <Icon name={icon}/>
          </div>
          <div className="label">{text}</div>
        </NavLink>
      </TooltipWrapper>
    );

    return null;
  }

  renderPopoverButton(title, content, HeaderPopoverPanel) {
    const renderPopoverContent = () => {
      return (
        <div className="masthead__popover">
          <div className="popover__title">{title}</div>
          <div className="masthead__popoverContent">
            Not implemented
          </div>
        </div>
      );
    };

    const preferredAlignment = {
      'horizontal': 'right'
    };

    return (
      <TooltipWrapper key={title.toLowerCase()} text={title}>
        <FixedPopoverTrigger isFixed preferredAlignment={preferredAlignment} renderPopoverContent={renderPopoverContent}>
          <NavButton content={content}/>
        </FixedPopoverTrigger>
      </TooltipWrapper>
    );
  }

  renderLinks() {
    return getHeaderLinks().map(link => {
      return this.renderNavLink(link.path, link.icon, link.text, link.condition, link.isExact);
    })
  }

  @bind
  handleUploadButtonClick() {
    this.context.router.history.push('/upload');
  }

  @bind
  handleLoginButtonClick() {
    this.props.ModalStore.addModal(LoginModal);
  }

  @bind
  handleRegisterButtonClick() {
    this.props.ModalStore.addModal(RegisterModal);
  }

  renderTools() {
    if (this.props.authUser) return this.renderAuthenticatedTools();

    return (
      <div className="tools">
        <div className="buttons">
          <Button onClick={this.handleLoginButtonClick} label="Log in"/>
          <Button onClick={this.handleRegisterButtonClick} label="Register"/>
        </div>
      </div>
    );
  }

  renderAuthenticatedTools() {
    const user = this.props.authUser;

    return (
      <div className="tools">
        <div className="buttons">
          <Button onClick={this.handleUploadButtonClick} label="Upload" icon="app.upload"/>
        </div>
        <UserButton user={user}/>
      </div>
    );
  }

  render() {
    return (
      <nav className="HeaderNav">
        <div className="links">{this.renderLinks()}</div>
        <div className="search"></div>
        {this.renderTools()}
      </nav>
    )
  }
}
