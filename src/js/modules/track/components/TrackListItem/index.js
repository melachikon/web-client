import {wrapInMediaQuery} from 'common/hoc';

import ExpensiveTrackListItem from './TrackListItem';
import CheapTrackListItem from './CheapTrackListItem';

const TrackListItem = wrapInMediaQuery('(max-width: 850px)')(CheapTrackListItem, ExpensiveTrackListItem);

export default TrackListItem;
