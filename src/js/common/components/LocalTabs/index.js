import preact from 'preact';

export default class LocalTabContext extends preact.Component {
  constructor(props) {
    super(props);

    this.state = {
      'currentTab': props.defaultTab,
    };
  }

  @bind
  handleSwitch(newTab) {
    this.setState({
      'currentTab': newTab,
    });

    if (this.props.onChange) this.props.onChange(newTab);
  }

  getChildContext() {
    return {
      'tabs': {
        'currentTab': this.state.currentTab,
        'switch': this.handleSwitch,
      }
    };
  }

  render() {
    return (
      <div className="LocalTabContext">
        {this.props.children}
      </div>
    );
  }
}
