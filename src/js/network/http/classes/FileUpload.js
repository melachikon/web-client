import axios from 'axios';

import {MediaServer} from 'network/http';
import {MediaServerConnection} from 'network/websocket';

import {observable} from 'mobx';

/** The file upload handles file uploading **/
export default class FileUpload {
  @observable file = null;
  @observable status = 'none';
  @observable progress = 0;
  @observable cancelToken = null;

  @observable id = null;
  @observable mediaId = null;

  constructor(options) {
    this.uploadTo = `/upload${options.uploadTo}`;

    MediaServerConnection.on('message', this.handleWebsocketMessage);
  }

  upload(file) {
    if (this.file) throw new Error('File upload already in progress!');

    const data = new FormData();
    data.append('file', file);

    this.file = file;
    this.status = 'uploading';
    this.cancelToken = axios.CancelToken.source();

    const promise = MediaServer.POST(this.uploadTo, data, {
      cancelToken: this.cancelToken.token,
      onUploadProgress: this.handleUploadProgress
    });

    promise.then(response => {
      const { id } = response.data;

      this.status = 'queued';
      this.id = id;
    });

    promise.catch(error => {
      this.status = 'error';
      this.file = null;
      this.id = null;
    });

    return promise;
  }

  @bind
  handleUploadProgress(event) {
    const percent = event.loaded / event.total;
    this.progress = percent;
  }

  @bind
  handleWebsocketMessage(message) {
    if (message.id !== this.id) return;

    const type = message.type;

    if (type == 'active') this.status = 'processing';
    if (type == 'completed') {
      this.status = 'completed';
      this.mediaId = message.mediaId;
    }
  }
}
