import preact from 'preact';
import {observer, inject} from 'mobx-react';

import Transition from 'react-addons-css-transition-group';
import TrackUploadItem from 'modules/track/components/TrackUploadItem';

@inject('TrackUploadStore') @observer
export default class TrackUploadList extends preact.Component {
  renderList() {
    return this.props.TrackUploadStore.items.map(item => {
      return (
        <TrackUploadItem key={item.id} item={item}/>
      )
    })
  }

  render() {
    return (
      <div className="TrackUploadList">
        <Transition transitionName="-anim" transitionLeaveTimeout={200}>
          {this.renderList()}
        </Transition>
      </div>
    );
  }
}
