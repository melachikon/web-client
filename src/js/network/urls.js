const isTunneled = true

const CLIENT_URL = isTunneled ? 'client-wavedistrict.ngrok.io' : 'localhost:3300'
const API_URL = isTunneled ? 'api.wavedistrict.com' : 'localhost:4000'
const MEDIA_URL = isTunneled ? 'media.wavedistrict.com' : 'localhost:4080'

export { CLIENT_URL, API_URL, MEDIA_URL }
