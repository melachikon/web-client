import preact from 'preact';
import classNames from 'classnames';
import {observer, inject} from 'mobx-react';

import ModalItem from 'modules/modal/components/ModalItem';
import Transition from 'react-addons-css-transition-group';

@inject('ModalStore') @observer
export default class ModalOverlay extends preact.Component {
  renderItems() {
    return this.props.ModalStore.items.map((modal) => {
      return <ModalItem modalKey={modal.key} {...modal}/>
    })
  }

  render() {
    const rootDivClassnames = classNames('modalOverlay', {
      'isActive': this.props.ModalStore.hasModals,
    });

    return (
      <div className={rootDivClassnames}>
        <Transition transitionName="anim__modal" transitionLeaveTimeout={200}>
          {this.renderItems()}
        </Transition>
      </div>
    );
  }
}
