import Database from 'common/classes/IndexedDB';

const ResourceStoreDB = new Database('ResourceStores', 3);
export default ResourceStoreDB;
