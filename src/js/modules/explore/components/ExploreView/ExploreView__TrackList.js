import preact from 'preact';

// Components
import TrackList from 'modules/track/components/TrackList';

export default class ExploreTrackList extends preact.Component {
  render() {
    const
    {resourceList} = this.props;

    return (
      <TrackList queueName="Explore" resourceList={resourceList}/>
    )
  }
}
