import preact from 'preact';

export default class Page extends preact.Component {
  render() {
    return (
      <div className={this.props.name || 'Page'}>
        {this.props.children}
      </div>
    );
  }
}
