import preact from 'preact';
import classNames from 'classnames';
import {observer, inject} from 'mobx-react';

// Router
import {NavLink} from 'react-router-dom'

// Hoc
import {wrapInLazyLoadHandler} from 'common/hoc';

// Components
import ImageRenderer from 'common/components/Image/ImageRenderer';
import CheapImage from 'common/components/CheapImage';
import ResourceImage from 'common/components/ResourceImage';

// Player components
import PlayButton from 'modules/player/components/PlayButton';
import Seeker from 'modules/player/components/Seeker';

// Helpers
import getImageSource from 'common/helpers/image/getImageSource';

const renderPlaceholder = () => {
  return (
    <div className="trackListItem__placeholder">
      <div className="trackListItem__placeholderArtwork"></div>
      <div className="trackListItem__placeholderBody"></div>
    </div>
  )
}

@wrapInLazyLoadHandler(renderPlaceholder)
@observer
export default class TrackListItem extends preact.Component {
  renderBackground(resource) {
    const hasBgArt = resource.media.bgartwork.exists;

    // Only render if active
    if (resource.isActive && !hasBgArt) {
      const blurArtworkProps = {
        'width': '500px',
        'height': '500px',
        'src': (getImageSource(resource.data.artwork) + '?preload=true'),
        'blurScale': 2,
      };

      return (
        <div className="trackListItem__blurArtwork">
          <ImageRenderer {...blurArtworkProps}/>
        </div>
      )
    }

    // Render always if it has background art
    if (hasBgArt) {
      const
      artworkProps = {
        'src': resource.media.bgartwork.normal,
        'scaling': 'cover',
      };

      return (
        <div className="trackListItem__backgroundArtwork">
          <CheapImage {...artworkProps}/>
        </div>
      )
    }

    return null;
  }

  render() {
    const
    {resource, index} = this.props,
    {data} = resource,
    rootDivClassnames = classNames('trackListItem', {
      'isActive': resource.isActive,
      'hasBgArt': resource.media.bgartwork.exists,
    });

    const
    artworkProps = {
      'width': '100%',
      'height': '100%',
      'name': 'artwork',
      'cheap': true,
      'resource': resource
    };

    return (
      <li data-id={resource.data.id} className={rootDivClassnames}>
        <div className="trackListItem__background">
          {this.renderBackground(resource)}
        </div>
        <div className="trackListItem__foreground isOverlay">
          <NavLink className="trackListItem__artwork" to={resource.clientLink}>
            <ResourceImage {...artworkProps}/>
          </NavLink>
          <div className="trackListItem__content">
            <div className="trackListItem__header">
              <div className="trackListItem__playButton">
                <PlayButton index={index} resource={resource}/>
              </div>
                <div className="trackListItem__primaryInfo">
                  <NavLink className="trackListItem__title" to={resource.clientLink}>
                    {data.title}
                  </NavLink>
                  <div className="trackListItem__type">
                    {data.type}
                  </div>
                </div>
                <div className="trackListItem__genreBadge">{data.genre}</div>
            </div>
            <div className="trackListItem__body"></div>
            <div className="trackListItem__footer">
              <div className="trackListItem__seeker">
                <Seeker resource={resource}/>
              </div>
            </div>
          </div>
        </div>
      </li>
    )
  }
}
