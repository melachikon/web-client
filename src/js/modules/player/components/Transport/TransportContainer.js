import preact from 'preact';
import {observer, inject} from 'mobx-react';

// HOC
import {injectPlayer} from 'modules/player/hoc';
import {wrapInMediaQuery} from 'common/hoc';

// Components
import Transition from 'react-addons-css-transition-group';

// Related components
import TransportBar from 'modules/player/components/TransportBar';
import TransportPanel from 'modules/player/components/TransportPanel';

const Transport = wrapInMediaQuery('(max-width: 960px)')(TransportPanel, TransportBar);

@injectPlayer
@observer
export default class TransportContainer extends preact.Component {
  render() {
    const {currentTrack} = this.props.player;

    if (!currentTrack) return null;

    return (
      <div>
        <div className="playerTransport__space"></div>
        <Transport key="transport" resource={currentTrack} player={this.props.player}/>
      </div>
    )
  }
}
