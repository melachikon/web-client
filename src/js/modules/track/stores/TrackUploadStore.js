import {observable} from 'mobx';

// Models
import {TrackUploadModel} from 'modules/track/models/';

class TrackUploadStore {
  @observable items = [];

  constructor() {
    this.add();
  }

  add() {
    const item = new TrackUploadModel();
    this.items.push(item);
  }

  remove(id) {
    this.items = this.items.filter(item => {
      return id !== item.id;
    })

    if (this.items.length < 1) this.add();
  }
}

const store = new TrackUploadStore();
export default store;
