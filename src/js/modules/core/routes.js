import preact from 'preact';
import {Route, Switch} from 'react-router-dom';

// Views
import HomeView from 'modules/home/components/HomeView';
import ExploreView from 'modules/explore/components/ExploreView';
import UserView from 'modules/user/components/UserView';
import TrackView from 'modules/track/components/TrackView';
import TrackUploadView from 'modules/track/components/TrackUploadView';

const ApplicationRoutes = () => {
  return (
    <div data-routes>
      <Route path="/" exact component={HomeView}/>
      <Switch>
        <Route path="/explore" component={ExploreView}/>
      </Switch>
      <Switch>
        <Route path="/upload" component={TrackUploadView}/>
      </Switch>
      <Switch>
        <Route path="/@:username/tracks/:slug" component={TrackView}/>
        <Route path="/@:username" component={UserView}/>
      </Switch>
    </div>
  )
};

export default ApplicationRoutes;
