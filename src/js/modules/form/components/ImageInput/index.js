import preact from 'preact';
import {observer, inject} from 'mobx-react';
import getClassName from 'classnames';

@observer
export default class ImageInput extends preact.Component {
  constructor(props) {
    super(props);

    this.controller = props.controller;
  }

  @bind handleChange(event) {
    const file = event.target.files[0];
    if (!file) return;

    this.controller.setValue(file);
    this.controller.read(file);
  }

  getImageStyle() {
    const renderedImageString = this.controller.renderedImageString;

    return {
      'background-image': `url(${renderedImageString})`,
    };
  }

  render() {
    const { controller } = this.props;

    const className = getClassName('ImageInput', {
      '-hasRendered': controller.hasRendered,
    });

    const inputProps = {
      'type': 'file',
      'onChange': this.handleChange,
    };

    return (
      <label className={className}>
        <div className="container">
          <div style={this.getImageStyle()} className="image"></div>
        </div>
        <div className="overlay"></div>
        <input {...inputProps} className="input"/>
      </label>
    );
  }
}
