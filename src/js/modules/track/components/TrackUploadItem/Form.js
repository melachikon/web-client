import preact from 'preact';

// Components
import FormContext from 'modules/form/components/FormContext';
import FormField from 'modules/form/components/FormField';

// Inputs
import TextInput from 'modules/form/components/TextInput';
import FileInput from 'modules/form/components/FileInput';
import ImageInput from 'modules/form/components/ImageInput';

export default class TrackUploadItemForm extends preact.Component {
  render() {
    const { form } = this.props;

    return (
      <FormContext form={form}>
        <div className="FormSidebar -left">
          <FormField name="artwork" label="Artwork" size="full">
            <ImageInput/>
          </FormField>
        </div>
        <div className="FormGrid">
          <FormField name="audio" label="Audio" size="half">
            <FileInput/>
          </FormField>
          <FormField name="slug" label="URL" size="half">
            <TextInput/>
          </FormField>
          <FormField name="title" label="Title" size="full">
            <TextInput/>
          </FormField>
          <FormField name="description" label="Description" size="full">
            <TextInput rows="5" multiline/>
          </FormField>
        </div>
      </FormContext>
    )
  }
}
