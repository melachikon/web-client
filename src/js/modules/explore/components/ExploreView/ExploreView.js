import preact from 'preact';
import {observer, inject} from 'mobx-react';

// HOC
import {redirect} from 'common/hoc';

// Components
import {LinkedTabs} from 'common/components/Tabs';
import Icon from 'common/components/Icon';

// Related components
import ExploreTrackList from './ExploreView__TrackList';
import ExploreUserList from './ExploreView__UserList';

// Routing
import {Route, Redirect, Switch} from 'react-router-dom';
import {matchUrl} from 'common/helpers/routing';

@redirect((props) => {
  const shouldRedirect = matchUrl(props.location.pathname, '/explore');
  if (shouldRedirect) return '/explore/tracks';

  return null;
})
@inject('ExploreStore') @observer
export default class ExploreView extends preact.Component {
  render() {
    const
    {ExploreStore} = this.props;

    const tabs = [
      {path: `/explore/tracks`, label: 'Tracks'},
      {path: `/explore/users`, label: 'Users'},
      {path: `/explore/collections`, label: 'Collections'},
    ];

    return (
      <div className="exploreView">
        <div className="pageHeader">
          <div className="pageTitle">
            <Icon name="app.explore"/>
            <h1>Explore</h1>
          </div>
          <div className="pageTabs">
            <LinkedTabs tabs={tabs}/>
          </div>
        </div>
        <div className="pageContent">
          <Switch>
            <Route path={tabs[0].path}>
              <ExploreTrackList resourceList={ExploreStore.tracks}></ExploreTrackList>
            </Route>
            <Route path={tabs[1].path}>
              <ExploreUserList resourceList={ExploreStore.users}></ExploreUserList>
            </Route>
          </Switch>
        </div>
      </div>
    )
  }
}
