import preact from 'preact';

// Import popovers
import CursorPopover from './CursorPopover';
import ElementPopover from './ElementPopover';

export default class PopoverContainer extends preact.Component {
  getPopoverComponent(type) {
    const typeToComponentMap = {
      'cursor': CursorPopover,
      'element': ElementPopover,
    };

    return typeToComponentMap[type] || CursorPopover;
  }

  render() {
    const PopoverComponent = this.getPopoverComponent(this.props.options.type);

    return (
        <PopoverComponent {...this.props}/>
    );
  }
}
