import preact from 'preact';

// Routing
import {NavLink} from 'react-router-dom';
import {getHeaderLinks} from 'modules/core/helpers/header';

import Icon from 'common/components/Icon';

export default class View extends preact.Component {
  renderNavLink(path, icon, text, condition, isExact) {
    if (condition) return (
      <NavLink onClick={this.props.onClose} exact={isExact} key={path} to={path} className="wd-link masthead__navLink" activeClassName="selected">
        <div className="masthead__navLinkIcon">
          <Icon name={icon}/>
        </div>
        <div className="masthead__navLinkText">{text}</div>
      </NavLink>
    );

    return null;
  }

  renderLinks() {
    return getHeaderLinks().map(link => {
      return this.renderNavLink(link.path, link.icon, link.text, link.condition, link.isExact);
    })
  }

  render() {
    return (
      <div>{this.renderLinks()}</div>
    )
  }
}
