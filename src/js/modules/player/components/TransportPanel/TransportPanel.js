import preact from 'preact';
import {observer, inject} from 'mobx-react';
import bem from 'bem-classname';

// Components
import ResourceImage from 'common/components/ResourceImage';
import Button from 'common/components/Button';
import Icon from 'common/components/Icon';
import ScrollContainer from 'common/components/ScrollContainer';

// Player components
import Seeker from 'modules/player/components/Seeker';
import PlayButton from 'modules/player/components/PlayButton';
import QueueList from 'modules/player/components/QueueList';

// Related components
import NowPlaying from './NowPlaying';

// Bind classes
const classNames = bem.bind(null, 'playerPanel');

@observer
export default class TransportPanel extends preact.Component {
  constructor() {
    super();

    this.state = {
      'isOpen': false,
      'showQueue': false,
    };
  }

  renderToggleButton() {
    const
    {player} = this.props,
    icon = player.isPlaying ? 'player.pause' : 'player.play';

    return (
      <Button icon={icon} onClick={player.toggle} type="TransportButton"/>
    )
  }

  @bind
  handleToggleOpen() {
    this.setState({
      'isOpen': !this.state.isOpen,
    });
  }

  @bind
  handleToggleQueue() {
    this.setState({
      'showQueue': !this.state.showQueue,
    });
  }

  renderBackground(resource) {
    const hasBackgroundArt = resource.media.bgartwork.exists;

    if (hasBackgroundArt) {
      return (
        <ResourceImage
          width="100%"
          height="100%"
          resource={resource}
          size="big"
          scaling="cover"
          name="bgartwork"
        />
      )
    }

    return (
      <ResourceImage
        width="100%"
        height="100%"
        blur={30}
        resource={resource}
        size="small"
        scaling="cover"
        name="artwork"
      />
    );
  }

  renderNowPlaying(resource, player) {
    return (
      <NowPlaying resource={resource}/>
    )
  }

  renderBody(resource, player) {
    if (this.state.showQueue) return this.renderQueue(resource, player);

    return this.renderNowPlaying(resource, player);
  }

  renderQueue(resource, player) {
    return (
      <div className={classNames('queueList')}>
        <ScrollContainer>
          <div className={classNames('queueListContent')}>
            <QueueList/>
          </div>
        </ScrollContainer>
      </div>
    )
  }

  renderQueueToggleIcon() {
    return this.state.showQueue ? "player.play" : "player.queue";
  }

  render() {
    const {resource, player} = this.props;

    const rootClassName = classNames({
      'isOpen': this.state.isOpen,
      'isPlaying': player.isPlaying,
      'isBuffering': player.isBuffering,
    });

    return (
      <div className={classNames('container')}>
        <div className={rootClassName}>
          <div className={classNames('background')}>
            {this.renderBackground(resource)}
          </div>
          <div className={classNames('foreground')}>
            <div className={classNames('header')}>
              <div className={classNames('toggleButton')}>
                <PlayButton className="playButton playButton--small" resource={resource}/>
              </div>
              <div className={classNames('summary')}>
                <div className={classNames('trackTitle')}>{resource.data.title}</div>
              </div>
              <div className={classNames('panelToggleSpace')}/>
            </div>
            <div className={classNames('panelToggle')}>
              <Button
                icon="app.chevron"
                onClick={this.handleToggleOpen}
                modifiers={{'playerPanelToggle': true}}
                type="TransportButton"
              />
            </div>
            <div className={classNames('openHeader')}>
              <div className={classNames('queueToggle')}>
                <Button
                  icon={this.renderQueueToggleIcon()}
                  onClick={this.handleToggleQueue}
                  modifiers={{'playerPanelToggle': true}}
                  type="TransportButton"
                />
              </div>
            </div>
            <div className={classNames('body')}>
              {this.renderBody(resource, player)}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
