import * as preact from 'preact'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import { Notification, NotificationContainer } from '../../src/js/common/components/Notification'
import '../../src/css/sass/default.scss'

class NotificationContainerTest extends preact.Component {
  notes = null

  render() {
    return (
      <div style={{ padding: '1rem' }}>
        <button onClick={() => this.notes.addNote('hi', 3000, action('clicked note'))}>
          Add note
        </button>
        <NotificationContainer ref={el => (this.notes = el)} />
      </div>
    )
  }
}

storiesOf('Notification', module)
  .add('default', () => {
    return <Notification text="This is a notification" />
  })
  .add('longer duration', () => {
    return <Notification text="This should last for 5000ms" duration={5000} />
  })
  .add('timeout callback', () => {
    return (
      <Notification
        text="I should log a timeout action to the console"
        onTimeout={action('timed out')}
      />
    )
  })
  .add('container', () => {
    return <NotificationContainerTest />
  })
