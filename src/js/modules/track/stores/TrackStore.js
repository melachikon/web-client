import {ReferencedResourceStore} from 'common/classes/Store';
import {TrackModel} from 'modules/track/models';

class TrackStore extends ReferencedResourceStore {
  Model = TrackModel

  getReferenceName(data) {
    return data.user.username + '_' + data.slug;
  }

  getReferenceUrl(ref) {
    const
    splittedRef = ref.split('_'),
    username = splittedRef[0],
    trackSlug = splittedRef[1];

    return `/users/${username}/tracks/${trackSlug}/`;
  }
}

const store = new TrackStore('/tracks', 'TrackStore');
export default store;
