import preact from 'preact';

import Icon from 'common/components/Icon';

export default class PageHeader extends preact.Component {
  render() {
    const { title, icon } = this.props;

    return (
      <div className="PageHeader">
        <div className="title">
          <Icon name={ icon }/>
          <h1>{ title }</h1>
        </div>
        <div className="bottom">
          { this.props.children }
        </div>
      </div>
    );
  }
}
