import preact from 'preact';
import {observer, inject} from 'mobx-react';
import getClassName from 'classnames';

@observer
export default class FormField extends preact.Component {
  getField() {
    return this.context.form.fields[this.props.name];
  }

  renderErrors() {
    const field = this.getField();

    return field.validationErrors.map(error => {
      return (
        <div className="errorItem">{ error }</div>
      );
    });
  }

  render() {
    const { label, size } = this.props;

    const child = preact.cloneElement(this.props.children[0], {
      'controller': this.getField(),
    });

    const className = getClassName('FormField', {
      [`-${size}`]: true,
    });

    return (
      <div className={className}>
        <label className="label">{ label }</label>
        <div className="input">
          { child }
        </div>
        <div className="errors">
          { this.renderErrors() }
        </div>
      </div>
    );
  }
}
