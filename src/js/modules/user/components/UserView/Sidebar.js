import preact from 'preact';

// Components
import ResourceImage from 'common/components/ResourceImage';
import Icon from 'common/components/Icon';

export default class UserViewSidebar extends preact.Component {
  renderDetailPoint(icon, strings, separator) {
    const hasData = strings.every(x => !!x);

    if (hasData) return (
      <div className="point">
        <div className="icon">
          <Icon name={icon}/>
        </div>
        <div className="text">{ strings.join(separator) }</div>
      </div>
    );

    return null;
  }

  render() {
    const { resource } = this.props;
    
    const {
      displayName,
      userType,
      bio,
      country,
      city,
      firstName,
      lastName,
      createdAt,
    } = resource.data;

    const avatarImageProps = {
      'resource': resource,
      'name': 'avatar',
      'width': '100%',
      'height': '100%',
    };

    return (
      <div className="UserViewSidebar">
        <div className="avatar">
          <ResourceImage {...avatarImageProps}/>
        </div>
        <div className="primaryInfo">
          <div className="displayName">{ displayName }</div>
          <div className="userType">{ userType }</div>
        </div>
        <div className="bio">
          { bio }
        </div>
        <div className="detailInfo">
          { this.renderDetailPoint('app.person', [firstName, lastName], ' ') }
          { this.renderDetailPoint('app.location', [city, country], ', ') }
          { this.renderDetailPoint('app.calendar', [createdAt], '') }
        </div>
      </div>
    );
  }
}
