import {computed} from 'mobx';

import {ResourceModel} from 'common/classes/Model';
import {parseMediaObject} from 'common/helpers/media';

// Stores
import {UserStore} from 'modules/user/stores';
import {ApplicationStore} from 'modules/core/stores';
import Player from 'player';

export default class Track extends ResourceModel {
  onInitialize(data) {
    this.clientLink = `/@${data.user.username}/tracks/${data.slug}`;
    this.user = UserStore.addItem(data.user);
  }

  onUpdate(data) {
    this.media = parseMediaObject(this.data.media, ['artwork', 'audio', 'bgartwork']);
  }

  @computed get isActive() {
    return (
      Player.currentTrack
      && Player.currentTrack.data.id == this.data.id
    );
  }

  @computed get isPlaying() {
    return (
      this.isActive && Player.isPlaying
    );
  }

  get currentTime() {
    if (this.isActive) return Player.currentTime;
    return 0;
  }

  get duration() {
    if (this.isActive) Player.source.audio.duration;
    return this.data.duration;
  }
}
