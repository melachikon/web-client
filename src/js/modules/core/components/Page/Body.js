import preact from 'preact';

export default class PageBody extends preact.Component {
  render() {
    return (
      <div className="PageBody">
        { this.props.children }
      </div>
    );
  }
}
