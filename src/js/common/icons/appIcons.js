import preact from 'preact';

export function hamburger() {
  return (
    <svg fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
    </svg>
  )
}

export function home() {
  return (
    <svg fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/>
    </svg>
  )
}

export function bell() {
  return (
    <svg fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.89 2 2 2zm6-6v-5c0-3.07-1.64-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.63 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2z"/>
    </svg>
  )
}

export function message() {
  return (
    <svg fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z"/>
    </svg>
  )
}

export function account() {
  return (
    <svg fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z"/>
    </svg>
  );
}

export function back() {
  return (
    <svg fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/>
    </svg>
  )
}

export function close() {
  return (
    <svg fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
    </svg>
  )
}

export function upload() {
  return (
    <svg fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M9 16h6v-6h4l-7-7-7 7h4zm-4 2h14v2H5z"/>
    </svg>
  )
}

export function explore() {
  return (
    <svg fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M12 10.9c-.61 0-1.1.49-1.1 1.1s.49 1.1 1.1 1.1c.61 0 1.1-.49 1.1-1.1s-.49-1.1-1.1-1.1zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm2.19 12.19L6 18l3.81-8.19L18 6l-3.81 8.19z"/>
    </svg>
  )
}

export function thumbsUp() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M1 21h4V9H1v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-.44-1.06L14.17 1 7.59 7.59C7.22 7.95 7 8.45 7 9v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5 1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-1.91l-.01-.01L23 10z"/>
    </svg>
  )
}

export function chevron() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
      <path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"/>
    </svg>
  )
}

export function camera() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
        <circle cx="12" cy="12" r="3.2"/>
        <path d="M9 2L7.17 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2h-3.17L15 2H9zm3 15c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5z"/>
    </svg>
  )
}

export function location() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.5 15">
      <path d="M9,1.5A5.25,5.25,0,0,0,3.75,6.75C3.75,10.69,9,16.5,9,16.5s5.25-5.81,5.25-9.75A5.25,5.25,0,0,0,9,1.5ZM9,8.63a1.88,1.88,0,1,1,1.88-1.87A1.88,1.88,0,0,1,9,8.63Z" transform="translate(-3.75 -1.5)" fill="#fff"/>
    </svg>
  )
}

export function person() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
      <path d="M9,9A3,3,0,1,0,6,6,3,3,0,0,0,9,9Zm0,1.5c-2,0-6,1-6,3V15H15V13.5C15,11.51,11,10.5,9,10.5Z" transform="translate(-3 -3)" fill="#fff"/>
    </svg>
  )
}

export function calendar() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 20">
      <path d="M19,3H18V1H16V3H8V1H6V3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3ZM12,6A3,3,0,1,1,9,9,3,3,0,0,1,12,6Zm6,12H6V17c0-2,4-3.1,6-3.1S18,15,18,17Z" transform="translate(-3 -1)" fill="#fff"/>
    </svg>
  )
}