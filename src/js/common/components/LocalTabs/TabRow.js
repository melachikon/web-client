import preact from 'preact';
import getClassName from 'classnames';

export default class TabRow extends preact.Component {
  renderTabButtons() {
    return this.props.tabs.map(tab => {
      const handleClick = () => {
        this.context.tabs.switch(tab.id);
      }

      const className = getClassName('link', {
        '-isActive': (this.context.tabs.currentTab == tab.id)
      });

      return (
        <a onClick={handleClick} className={className}>
          {tab.label}
        </a>
      );
    })
  }

  render() {
    return (
      <div className="TabRow">
        {this.renderTabButtons()}
      </div>
    );
  }
}
