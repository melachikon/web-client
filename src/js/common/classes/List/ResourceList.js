import {action, observable} from 'mobx'
import moment from "moment";

import {ResourceStore} from 'common/classes/Store';
import {ApiServer} from 'network/http';

export default class ResourceList {
  Store = ResourceStore;

  url = '';
  filters = {};

  @observable count = null;
  @observable timestamp = null;

  @observable limit = 25;
  @observable offset = 0;

  constructor(url, Store, onAdd) {
    this.url = url;

    this.Store = Store || this.Store;
    this.onAdd = onAdd || this.Store.addItem;

    this.timestamp = moment().toISOString();
    this.pendingPromise = null;
  }

  @action
  addItems(listOfItems) {
    return listOfItems.map(item => {
      return this.onAdd(item);
    })
  }

  /** Infinite scroll **/
  @observable items = [];
  @observable status = 'none';
  @observable hasInitial = false;
  @observable hasMore = false;

  @bind
  @action
  clear() {
    this.offset = 0;
    this.items = [];
    this.hasMore = false;
    this.hasInitial = false;
  }

  @bind
  @action
  refresh() {
    this.clear();
    this.timestamp = moment().toISOString();
    this.fetch();
  }

  @bind
  @action
  fetch() {
    if (this.status == 'pending') return this.pendingPromise;

    const queries = {
      ...this.filters,
      'offset': this.offset,
      'limit': this.limit,
      'olderThan': this.timestamp,
    };

    this.status = 'pending';

    const promise = this.pendingPromise = ApiServer.GET(this.url, {
      'params': queries
    });

    promise.then(response => {this.handleSuccessfulResponse(response)});

    return promise;
  }

  @action
  handleSuccessfulResponse(response) {
    if (!response) throw new Error('Expected request response')
    if (!response.data || !response.data.rows) throw new Error('Expected response rows')

    this.items.push(...this.addItems(response.data.rows));

    this.hasInitial = true;
    this.hasMore = !!response.data.hasNext;

    this.status = 'fulfilled'
    this.offset = this.offset + this.limit;
    this.count = response.data.count;
  }
}
