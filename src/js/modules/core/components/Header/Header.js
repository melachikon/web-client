import preact from 'preact';
import bem from 'bem-classname';
import {observer, inject} from 'mobx-react';

// Router
import {Link} from "react-router-dom";

// HOC
import {wrapInMediaQuery} from 'common/hoc/';

// Components
import HeaderNav from 'modules/core/components/HeaderNav';
import HamburgerNav from 'modules/core/components/HamburgerNav';

// HOC Wrappers
const Nav = wrapInMediaQuery('(max-width: 800px)')(HamburgerNav, HeaderNav);

@inject('ApplicationStore') @observer
export default class Header extends preact.Component {
  render() {
    return (
      <div className="Header">
        <header className="element">
          <div className="inner">
            <Link className="logoContainer" to={'/'}>
              <div className="logo">
                  <img title="WaveDistrict" alt="WaveDistrict" src="/img/logo/vector.svg" height="25" width="25"/>
              </div>
            </Link>
            <Nav {...this.props}/>
          </div>
        </header>
      </div>
    )
  }
}
