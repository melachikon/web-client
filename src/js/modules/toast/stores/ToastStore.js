import { action, observable } from 'mobx'

function noop() {}

class ToastModel {
  constructor(content, duration, clickAction) {
    this.id = window.performance.now()
    this.content = content
    this.duration = duration
    this.clickAction = clickAction
  }
}

class ToastStore {
  @observable toasts = []

  @action
  addToast(content, duration = Infinity, clickAction = noop) {
    this.toasts.push(new ToastModel(content, duration, clickAction))
  }

  @action
  removeToast(id) {
    this.toasts = this.toasts.filter(note => note.id !== id)
  }
}

export default new ToastStore()
