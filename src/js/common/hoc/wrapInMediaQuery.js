import preact from 'preact';

const wrapInMediaQuery = (query) => (TrueComponent, FalseComponent) => {
  class ResponsiveComponent extends preact.Component {
    constructor() {
      super();

      this.state = {
        'isMatching': false
      };

      this.handleResize = this.handleResize.bind(this);
    }

    componentWillMount() {
      this.queryList = window.matchMedia(query);
      this.setMatchState(this.queryList.matches);

      this.queryList.addListener(this.handleResize);
    }

    componentWillUnmount() {
      this.queryList.removeListener(this.handleResize);
    }

    setMatchState(isMatching) {
      this.setState({
        'isMatching': isMatching
      });
    }

    handleResize(queryList) {
      this.setMatchState(queryList.matches);
    }

    render() {
      if (this.state.isMatching && TrueComponent) return <TrueComponent {...this.props}/>;
      if (!this.state.isMatching && FalseComponent) return <FalseComponent {...this.props}/>;

      return null;
    }
  }

  return ResponsiveComponent;
}

export default wrapInMediaQuery;
