import React from 'react';
import {Route} from 'react-router-dom';

// Views
import HomeView from 'modules/home/components/HomeView';

const HomeRoutes = () => {
  return (
    <Route path="/" exact component={HomeView}/>
  )
};

export default HomeRoutes;
