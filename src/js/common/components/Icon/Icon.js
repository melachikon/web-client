import preact from 'preact';

import icons from 'common/icons';

export default class Icon extends preact.Component {
  getIcon(iconName) {
    const splitIconName = iconName.split('.');
    return icons[splitIconName[0]][splitIconName[1]]();
  }

  render({name}) {
    if (name) return (
      <icon>
        {this.getIcon(name)}
      </icon>
    );
  }
}
