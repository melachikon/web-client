import preact from 'preact';
import bem from 'bem-classname';

// Components
import ResourceImage from 'common/components/ResourceImage';

// Player components
import Seeker from 'modules/player/components/Seeker';
import TransportControls from './TransportControls';

// Bind classes
const classNames = bem.bind(null, 'playerNowPlaying');

export default class NowPlaying extends preact.Component {
  render() {
    const {resource} = this.props;

    return (
      <div className="playerNowPlaying">
        <div className="playerNowPlaying__artwork">
          <ResourceImage
            width="100vw"
            height="100vw"
            resource={resource}
            size="normal"
            scaling="contain"
            name="artwork"
          />
        </div>
        <div className={classNames('controls')}>
          <div className={classNames('controlTransport')}>
            <TransportControls resource={resource}/>
          </div>
          <div className={classNames('controlSeeker')}>
            <Seeker resource={resource}/>
          </div>
          <div className={classNames('controlSummary')}>
            <div className={classNames('controlTrackTitle')}>{resource.data.title}</div>
            <div className={classNames('controlTrackUser')}>{resource.user.data.display_name}</div>
          </div>
        </div>
      </div>
    )
  }
}
