import preact from 'preact';
import classNames from 'classnames';
import {observer, inject} from 'mobx-react';

@inject('ModalStore') @observer
export default class ModalItem extends preact.Component {
  getContentProps() {
    const dismiss = () => {
      this.props.ModalStore.dismissModal(this.props.modalKey);
    };

    return {
      ...this.props,
      'dismiss': dismiss,
    };
  }

  renderContent() {
    const { renderModalContent } = this.props;

    if (renderModalContent.prototype instanceof preact.Component) {
      const Component = renderModalContent;

      return <Component {...this.getContentProps()}/>;
    }

    if (typeof renderModalContent == 'function') {
      return renderModalContent(this.getContentProps());
    }

    return 'Invalid modal content';
  }

  render() {
    const { title } = this.props;

    let type = this.props.type || 'ModalItem';

    return (
      <div className={type}>
        { this.renderContent() }
      </div>
    )
  }
}
