import preact from 'preact';
import {observer, inject} from 'mobx-react';
import bem from 'bem-classname';

// Components
import ResourceImage from 'common/components/ResourceImage';
import SliderInput from 'common/components/SliderInput';
import Button from 'common/components/Button';
import FixedPopoverTrigger from 'common/components/FixedPopoverTrigger';
import TooltipWrapper from 'common/components/TooltipWrapper';
import ScrollContainer from 'common/components/ScrollContainer';
import Icon from 'common/components/Icon';

// Player components
import Seeker from 'modules/player/components/Seeker';
import QueueList from 'modules/player/components/QueueList';
import PlayButton from 'modules/player/components/PlayButton';

// Router
import {NavLink} from 'react-router-dom';

// Bind classes
const classNames = bem.bind(null, 'playerTransport');

@observer
export default class TransportBar extends preact.Component {
  renderQueueTitle() {
    const
    {player} = this.props,
    {queue} = player,
    currentItem = queue.currentItem;

    if (currentItem) return currentItem.name;
    return 'No queue';
  }

  renderToggleButton() {
    const
    {player} = this.props,
    icon = player.isPlaying ? 'player.pause' : 'player.play';

    return (
      <Button icon={icon} onClick={player.toggle} type="TransportButton"/>
    )
  }

  // Render the queue popover
  @bind renderQueueList() {
    return (
      <div className={classNames('queueList')}>
        <div className="popover__title">Queue</div>
        <div className="popover__content">
          <ScrollContainer>
            <div className={classNames('queueListContent')}>
              <QueueList/>
            </div>
          </ScrollContainer>
        </div>
      </div>
    );
  }

  renderQueueButton() {
    return (
      <TooltipWrapper text="Queue">
        <FixedPopoverTrigger isFixed renderPopoverContent={this.renderQueueList}>
          <Button icon="player.queue" type="TransportButton"/>
        </FixedPopoverTrigger>
      </TooltipWrapper>
    )
  }

  // Render next/previous preview
  renderTrackPreview(resource) {
    return (
      <div>{resource.data.title}</div>
    )
  }

  // Transport controls
  renderRepeatButton() {
    return (
      <Button icon="player.repeat" onClick={Player.toggleRepeat} isActive={Player.isRepeating} type="TransportButton"/>
    )
  }

  renderPreviousButton() {
    return (
      <Button icon="player.previous" onClick={Player.previous} type="TransportButton"/>
    )
  }

  renderPlayButton() {
    return (
      <PlayButton
        className="playButton playButton--small"
        resource={this.props.resource}
      />
    )
  }

  renderNextButton() {
    return (
      <Button icon="player.next" onClick={Player.next} type="TransportButton"/>
    )
  }

  renderShuffleButton() {
    return (
      <Button icon="player.shuffle" onClick={Player.toggleShuffle} isActive={Player.isShuffling} type="TransportButton"/>
    )
  }

  // Rendering
  render() {
    const {resource, player} = this.props;

    return (
      <div className={classNames('container')}>
        <div className={classNames({'isPlaying': player.isPlaying, 'isBuffering': player.isBuffering})}>
          <div className={classNames('controls')}>
            <div className="playerControls">
              <div className="playerControls__prev">
                {this.renderPreviousButton()}
              </div>
              <div className="playerControls__toggle">
                {this.renderPlayButton()}
              </div>
              <div className="playerControls__next">
                {this.renderNextButton()}
              </div>
            </div>
          </div>
          <div className={classNames('seeker')}>
            <Seeker resource={resource}/>
          </div>
          <div className={classNames('controls')}>
            <div className="playerControls">
              <div className="playerControls__shuffle">
                {this.renderShuffleButton()}
              </div>
              <div className="playerControls__repeat">
                {this.renderRepeatButton()}
              </div>
            </div>
          </div>
          <div className={classNames('volume')}>
            <Button icon="player.volume" type="TransportButton"/>
            <SliderInput value={player.volume} onMove={player.setVolume}/>
          </div>
          <div className={classNames('trackSummary')}>
            <NavLink to={resource.clientLink} className={classNames('trackArtwork')}>
              <ResourceImage width="35px" height="35px" resource={resource} size="tiny" name="artwork"/>
            </NavLink>
            <div className={classNames('trackInfo')}>
              <NavLink to={resource.clientLink} className={classNames('trackTitle')}>
                {resource.data.title}
              </NavLink>
              <div className={classNames('queueInfo')}>
                {this.renderQueueTitle()}
              </div>
            </div>
          </div>
          <div className={classNames('actions')}>
            {this.renderQueueButton()}
          </div>
        </div>
      </div>
    )
  }
}
