import preact from 'preact';

// Components
import Button from 'common/components/Button';

// Routing
import {Route, Redirect, Switch} from 'react-router-dom';
import {LinkedTabs} from 'common/components/Tabs';
import {matchUrl} from 'common/helpers/routing';

// Sub Views
import UserTrackList from './UserView__TrackList';

export default class UserViewContent extends preact.Component {
  getTabs() {
    const { resource } = this.props;

    return [
      {
        path: `${resource.clientLink}/tracks`,
        label: 'Tracks',
        component: UserTrackList,
      },
      {
        path: `${resource.clientLink}/collections`,
        label: 'Collections',
        component: 'div',
      },
    ];
  }

  renderRoutes() {
    const tabs = this.getTabs();

    return tabs.map(tab => {
      const Component = tab.component;

      return (
        <Route path={tab.path}>
          <Component {...this.props}/>
        </Route>
      );
    });
  }

  render() {
    const { resource } = this.props;
    const tabs = this.getTabs();

    return (
      <div className="UserViewContent">
        <div className="buttons">
          <Button label="Follow"/>
          <Button label="Message"/>
          <Button label="More"/>
        </div>
        <div className="tabs">
          <LinkedTabs tabs={tabs}/>
        </div>
        <div className="body">
          <Switch>
            { this.renderRoutes() }
            <Redirect from={resource.clientLink} to={`${resource.clientLink}/tracks`}/>
          </Switch>
        </div>
      </div>
    )
  }
}
