export function convertSecondsToHHMMSS(s1) {
  s1 = Math.round(s1)
  var h1 = Math.floor(s1/(60 * 60));
    s1 %= 60 * 60;
    var m1 = Math.floor(s1/60);
    s1 %= 60;
    var h2 = h1 ? h1+':' : '',
        m2 = h1 && m1<10 ? '0'+m1 : m1,
        s2 = s1<10 ? '0'+s1 : s1;
    return h2 + m2 + ':' + s2;
}
