import preact from 'preact';
import {observer, inject} from 'mobx-react';

// Components
import LocalTabs from 'common/components/LocalTabs';
import TabRow from 'common/components/LocalTabs/TabRow'
import Button from 'common/components/Button';

// Related components
import Form from './Form';
import Status from './Status';

@observer
export default class TrackUploadItem extends preact.Component {
  @bind
  handleTabChange(newTab) {
    this.props.item.currentTab = newTab;
  }

  @bind
  handleSaveClick() {
    this.props.item.save();
  }

  render() {
    const { currentTab, form } = this.props.item;

    const tabs = [
      {id: 'basic', label: 'Basic Info'},
      {id: 'captions', label: 'Captions'},
      {id: 'settings', label: 'Settings'},
    ];

    return (
      <div className="TrackUploadItem">
        <LocalTabs onChange={this.handleTabChange} defaultTab={currentTab}>
          <div className="tabs">
            <TabRow tabs={tabs}/>
          </div>
          <div className="content">
            <Form form={form}/>
          </div>
          <div className="footer">
            <div className="status">
              <Status item={this.props.item}/>
            </div>
            <div className="buttons">
              <Button isDisabled={this.props.item.autoSubmit} onClick={this.handleSaveClick} label="Save"/>
              <Button label="Remove"/>
            </div>
          </div>
        </LocalTabs>
      </div>
    );
  }
}
