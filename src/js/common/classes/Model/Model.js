import {observable, computed, action} from 'mobx';
import moment from 'moment';

export default class Model {
  @observable data = {};

  @observable dateCreated = null;
  @observable dateModified = null;

  @observable hasInitialData = false;

  constructor() {
    this.dateCreated = moment().toISOString();
  }

  // Called when data is set
  onUpdate() {}

  // Called when data is set the first time only
  onInitialize() {}

  setData(data) {
    this.data = Object.assign(this.data, data);
    this.dateModified = moment().toISOString();

    if (!this.hasInitialData) this.onInitialize(data);
    this.onUpdate(data);

    this.hasInitialData = true;
  }
}
