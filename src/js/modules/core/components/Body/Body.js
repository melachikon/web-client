import preact from 'preact';
import {observer, inject} from 'mobx-react';

export default class Body extends preact.Component {
  render() {
    return (
      <main className="body__content">
        {this.props.routerChildren}
      </main>
    )
  }
}
